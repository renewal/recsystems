FROM python:3.7-slim-buster

# Use tini so we can easily stop the recsystem process with SIGTERM, etc.
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini \
    /usr/local/bin/tini
RUN chmod +x /usr/local/bin/tini

WORKDIR /usr/src/app
RUN apt-get update && apt-get install -yqq git && rm -rf /var/lib/apt/lists/*
# pip needs to be upgraded before some dependencies can be installed properly
RUN pip install --no-cache-dir --upgrade pip
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
RUN pip install -e . && rm -rf .git/
ENTRYPOINT [ "/usr/local/bin/tini", "--", "python", "-m", "renewal_recsystem" ]
