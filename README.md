# Renewal Recsystems

[![Documentation Status](https://readthedocs.org/projects/renewal-recsystems/badge/?version=latest)](https://renewal-recsystems.readthedocs.io/en/latest/?badge=latest)

This repository provides documentation for implementing a recommendation
system ("recsystem" for short) compatible with the Renewal competition
platform, as well as a reference implementation in Python which can be
extended to implement your own recsystem.

You can implement your recsystem in several ways:

1. Use the high-level interface of the reference implementation.  This
   allows you to write your recsystem by creating a Python file containing a
   few functions that follow a pre-defined format.  The reference system
   does the rest of the heavy lifting while you focus on your recommendation
   algorithm.  This is the easiest way to get started.  See the [Quickstart
   Guide](https://renewal-recsystems.readthedocs.io/en/latest/quickstart.html).

2. More experienced Python coders who find the high-level interface too
   limiting may wish to explore the lower-level reference implementation
   provided by the `renewal_recsystem.RenewalRecsystem` base class.  This
   provides a lot of boilerplate functionality such as handling of the
   WebSocket connection and JSON-RPC details, so you can focus on just the
   details of your recommendation algorithms.  However, it requires more
   experience with object-oriented programming and asyncio.

3. You may implement a recsystem completely from scratch, either in Python
   or any other language, as the protocol for recsystems is based on open
   standards.  The [full
   documentation](https://renewal-recsystems.readthedocs.io/en/latest/#id3)
   provides the details needed to do this.  Reference implementations in new
   languages would be a welcome contribution to the project, but may be
   outside the scope of a single competition (depending on the length of the
   competition).

A recommendation system can be written in any language: It connects to the
Renewal Backend over HTTP(S) and communicates with it using standard web
technologies, namely [WebSockets](https://en.wikipedia.org/wiki/WebSocket)
using the [JSON-RPC](https://en.wikipedia.org/wiki/JSON-RPC) protocol.

The Backend also provides a
[RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) API
against which recsystems can make additional calls for data lookups (e.g. to
fetch articles and user histories).

The REST and JSON-RPC interfaces, and all other details of implementing a
recsystem are documented in detail in the [full
documentation](https://renewal-recsystems.readthedocs.io/en/latest/#id3).

Sample implementations in additional languages (e.g. JavaScript) may be
added in the future.


## Installation

Prerequisites: Python 3.7 or greater.

Using a Python "virtual environment" to install the package is highly
recommended, as this ensures it will be installed in an isolated environment
that will not conflict with the rest of your Python installation.  See
[Creating a virtual
environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment).

To install the `renewal_recsystem` package, first clone this git repository:

```bash
$ git clone https://gitlab.lisn.upsaclay.fr/renewal/recsystems.git
$ cd recsystems/
```

Then from the root of the repository, install it with:

```bash
$ pip install .
```

Alternatively, this can be done in a single command like:

```bash
$ pip install git+https://gitlab.lisn.upsaclay.fr/renewal/recsystems.git
```

For installing to do development on the package itself, install in
"editable" mode like:

```bash
$ pip install -e .
```

The package is not currently published on a package index like PyPI, but
may be in the future.

You can make a quick check that the installation worked by running:

```bash
$ python -m renewal_recsystem --help
```


### Additional steps

The following steps are good to check if you plan to do development on this
package; if you only intend to use it for implementing your own recsystem
it is not necessary to run the tests or build the documentation.

#### Running the tests

To run the tests, first install the test dependencies by changing
directories to the root of the repository and running:

```bash
$ pip install -e .[tests]
```

Then simply run the tests with `pytest` like:

```bash
$ pytest
```


#### Building the documentation

To build the documentation for this repository, first install the
documentation dependencies by changing directories to the root of the
repository and running:

```bash
$ pip install -e .[docs]
```

Then run

```bash
$ cd docs/
$ make html
```

The HTML docs will be output to `_build/html`.


## Baseline recommendation system

This package also implements the baseline recsystem service used by the
renewal backend to provide recommendations as a baseline against which
contest participant recsystems can be compared, and which provides backup
recommendations when participant recsystems become unreachable.

### Running the baseline recsystem

To run the baseline recsystem, install the package, then run:

```
$ python -m renewal_recsystem --token=<token> renewal_recsystem.baseline.popularity
```

or

```
$ python -m renewal_recsystem --token=<token> renewal_recsystem.baseline.random
```

The only required flag is `--token` to provide the authentication token
for the recsystem (this is provided by an administrator when registering
this recsystem with the backend).  The token is a [JSON Web
Token](https://jwt.io/) (JWT) and can be provided either directly as a
string, or as the path to a file containing the JWT and nothing else
(recommended).

The baseline takes some other optional command line parameters which can be
listed by running:

```
$ python -m renewal_recsystem --help
```

The positional parameter is the name of the Python module which implements
the recommendation system itself:

* random: articles are simply returned at random

* popularity: for every batch of articles requested, the articles are
  returned prioritized by "popularity" as measured by their number of clicks
  and their overall rating by users

Each command-line flag can also be passed as an environment variable
instead.  The associated environment variable is the same as the flag but
all uppercase and prefixed with `RENEWAL_`.  For example, instead of passing
`--token=<token>` you can set the environment variable
`RENEWAL_TOKEN=<token>`.

New recsystems can be implemented by providing your own Python module
defining some basic callback functions; see [full
documentation](https://renewal-recsystems.readthedocs.io/en/latest/#id3) for
more details.
