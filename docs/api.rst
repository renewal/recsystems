.. module:: renewal_recsystem
.. currentmodule:: renewal_recsystem


``renewal_recsystem`` API Documentation
=======================================

This page contains the full generated API documentation for objects in the
`renewal_recsystem` package.

For documentation on how recsystems interact with the Renewal backend, see
the :ref:`backend-api`.  For more narrative documentation on the high-level
interface for writing recsystems, see :doc:`interface`.

.. autosummary::

    basic
    types
    recsystem
    articles
    server
    utils
    utils.testing


``renewal_recsystem.basic``
---------------------------

.. automodule:: renewal_recsystem.basic
    :members:


``renewal_recsystem.types``
---------------------------

.. automodule:: renewal_recsystem.types
    :members:


``renewal_recsystem.recystem``
------------------------------

.. automodule:: renewal_recsystem.recsystem
    :members:


``renewal_recsystem.articles``
------------------------------

.. automodule:: renewal_recsystem.articles
    :members:


``renewal_recsystem.server``
------------------------------

.. automodule:: renewal_recsystem.server
    :members:


``renewal_recsystem.utils``
------------------------------

.. automodule:: renewal_recsystem.utils
    :members:
    :imported-members:


``renewal_recsystem.utils.testing``
-----------------------------------

.. automodule:: renewal_recsystem.utils.testing
    :members:
