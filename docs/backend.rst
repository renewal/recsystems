.. _backend-api:

Renewal Backend API
===================

Introduction
------------


.. _http-api:

HTTP API
--------

These are the RESTful interfaces on the Renewal backend currently available
to recsystems, e.g. to download details of articles, fetch currently
assigned users, etc.

All URIs are currently relative to ``/v1`` as there is currently only
one API version.

.. http:get:: /v1

    Returns the current API version.

    Used just to check that the API is up.

    **Example request:**

    .. sourcecode:: http

        GET /v1 HTTP/1.1
        Host: api.renewal-research.com
        Accept: application/json

    **Example response:**

    .. sourcecode:: http

        HTTP/1.1 200
        Content-Type: application/json

    .. sourcecode:: json

        {"version": 1}

    :statuscode 200: success

.. http:get:: /v1/articles

    Fetch a list of article documents from the backend.

    Articles are formatted according to the :ref:`data-structure-article`
    data type.

    Although the WebSocket interface will continuously provide your
    recsystem with new articles as they are crawled, this allows your
    recsystem to fetch all the details about past articles, e.g. for
    training purposes, or for pre-fetching a list of recent articles when
    the recsystem first comes online (e.g. in
    `~renewal_recsystem.recsystem.RenewalRecsystem.initialize`).

    **Example request:**

    .. sourcecode:: http

        GET /v1/articles?max_id=11000&limit=1 HTTP/1.1
        Host: api.renewal-research.com
        Accept: application/json
        Authorization: Bearer <token>

    **Example response:**

    .. note::

        The real reponse contains the full article summary and text, but
        they are truncated in this example.

    .. sourcecode:: http

        HTTP/1.1 200
        Content-Type: application/json

    .. sourcecode:: json

         [
          {
            "article_id": 10999,
            "authors": [
              "Brooks Barnes"
            ],
            "publish_date": "2020-09-30T01:14:41",
            "image_url": "https://static01.nyt.com/images/2020/09/25/business/25virus-disneyparks-3/25virus-disneyparks-3-facebookJumbo.jpg",
            "keywords": [
              "workers",
              "unionized",
              "world",
              "newsom",
              "disneyland",
              "mr",
              "theme",
              "quarter",
              "lays",
              "florida",
              "park",
              "disney",
              "restrictions"
            ],
            "lang": "en",
            "metrics": {
              "bookmarks": 0,
              "clicks": 0,
              "dislikes": 0,
              "likes": 0
            },
            "site": {
              "icon_url": "http://localhost:8080/v1/images/icons/5f68e3404b19bc8dd873ef25",
              "name": "NYTimes",
              "url": "www.nytimes.com"
            },
            "summary": "In Florida, where government officials have ...",
            "text": "Disneyland in California has remained closed ...",
            "title": "Disney Lays Off a Quarter of U.S. Theme Park Workers",
            "url": "https://www.nytimes.com/2020/09/29/business/disney-theme-park-workers-layoffs.html"
          }
        ]

    :query limit: the maximum number of articles to return; note that this
     may be limited to an internal upper-limit which is not currently
     specified
    :query max_id: the maximum (exclusive) ``article_id`` to return; all
     returned articles will have ``article_id`` less than this
    :query since_id: the minimum (exclusive) ``article_id`` to return; all
     returned articles will have ``article_id`` greater than this
    :reqheader Authorization: authentication token in ``Bearer <token>``
     format
    :statuscode 200: success
    :statuscode 401: unauthorized; missing or invalid authentication token

.. http:get:: /v1/articles/(int:article_id)

    Fetch a single of article document from the backend.

    This is like ``/v1/articles/`` but just returns a single article by
    ``article_id``.

    Articles are formatted according to the :ref:`data-structure-article`
    data type.

    **Example request:**

    .. sourcecode:: http

        GET /v1/articles/10999 HTTP/1.1
        Host: api.renewal-research.com
        Accept: application/json
        Authorization: Bearer <token>

    **Example response:**

    .. sourcecode:: http

        HTTP/1.1 200
        Content-Type: application/json

    .. sourcecode:: json

        {
          "article_id": 10999,
          "summary": "In Florida, where government officials have ...",
          "text": "Disneyland in California has remained closed ...",
          "title": "Disney Lays Off a Quarter of U.S. Theme Park Workers",
          "url": "https://www.nytimes.com/2020/09/29/business/disney-theme-park-workers-layoffs.html"
          ...
        }

    :reqheader Authorization: authentication token in ``Bearer <token>``
     format
    :statuscode 200: success
    :statuscode 401: unauthorized; missing or invalid authentication token
    :statuscode 404: article does not exist

.. http:get:: /v1/articles/interactions/(int:article_id)

    Return all user interactions on the article with the specified
    ``article_id``.  May optionally filter by ``user_id`` to get a single
    result (as an ``object`` instead of ``array``) for that user's
    interactions with the article.

    **Example request:**

    .. sourcecode:: http

        GET /v1/articles/interactions/10999 HTTP/1.1
        Host: api.renewal-research.com
        Accept: application/json
        Authorization: Bearer <token>

    **Example response:**

    .. sourcecode:: http

        HTTP/1.1 200
        Content-Type: application/json

    .. sourcecode:: json

        [
          {
            "article_id": 1452,
            "prev_rating": 1,
            "rating": 1,
            "user_id": "Vf7tIKw9uMQRiZ40v6wnNBcVI2G3",
            "when": "2020-09-08T13:46:47.119Z"
          },
          {
            "article_id": 1452,
            "bookmarked": true,
            "clicked": true,
            "prev_rating": 0,
            "rating": 1,
            "user_id": "Mhkc4xuaFPWnmbFomIv8drAtsn13"
          }
        ]

    :query user_id: optionally filter by ``user_id``, in this case only
     one result ``object`` is returned if found
    :reqheader Authorization: authentication token in ``Bearer <token>``
     format
    :statuscode 200: success
    :statuscode 401: unauthorized; missing or invalid authentication token
    :statuscode 404: no article interactions for the given article and/or
     user were found

.. http:get:: /v1/user_assignments

    Get the user IDs of all users currently assigned to your recsystem, as
    well as their past article interaction history.

    **Example request:**

    .. sourcecode:: http

        GET /v1/user_assignments HTTP/1.1
        Host: api.renewal-research.com
        Accept: application/json
        Authorization: Bearer <token>

    **Example response:**

    .. sourcecode:: http

        HTTP/1.1 200
        Content-Type: application/json

    .. sourcecode:: json

        [
          {
            "user_id": "Mhkc4xuaFPWnmbFomIv8drAtsn13",
            "interactions": [{"article_id": 1234, "recommended": true}]
          },
          {
            "user_id": "ct4LvjwHDOXdIGH1kJUtAvVQgmv1",
            "interactions": []
          }
        ]

    :reqheader Authorization: authentication token in ``Bearer <token>``
     format
    :statuscode 200: success
    :statuscode 401: unauthorized; missing or invalid authentication token


.. _json-rpc-api:

JSON-RPC API
------------

.. currentmodule:: renewal_recsystem.backend


This documents the JSON-RPC methods that the Renewal backend may make to
your recsystem, and which must be implemented by your recsystem.

They are documented here as though they were implemented as Python functions
but the function signatures and meanings of the parameters are transferable
to any implementation language.  The types of parameters and return values
are given in their corresponding Pythons types.  To determine what they would
be in a different implementation language, you can map the Python types to
the corresponding JSON types.  Then look up what those types correspond to
in your implementation language:

============ ==========
    JSON       Python
============ ==========
``string``     `str`
``integer``    `int`
``number``     `float`
``array``      `list`
``object``     `dict`
``null``       `None`
============ ==========

Notification methods are indicated in the description, and do not return any
value.

.. autosummary::

    ping
    stats
    recommend
    new_article
    article_interaction
    assigned_user
    unassigned_user


.. autofunction:: ping
.. autofunction:: stats
.. autofunction:: recommend


Notifications
^^^^^^^^^^^^^

.. autofunction:: new_article
.. autofunction:: article_interaction
.. autofunction:: assigned_user
.. autofunction:: unassigned_user


Data Structures
---------------

This documentation describes the formats of data structures sent to your
recsystem by the HTTP and JSON-RPC APIs used by the Renewal backend.

The data structures described first in `JSON Schema`_ format, and each is
followed by an example.


.. _data-structure-article:

Article
^^^^^^^

.. _data-structure-article-interaction:

Article Interaction
^^^^^^^^^^^^^^^^^^^


.. include:: links.txt
