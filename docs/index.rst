.. Renewal Recsystems documentation master file, created by
   sphinx-quickstart on Wed Nov 25 18:06:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

******************
Renewal Recsystems
******************

This documentation provides the necessary information for implementing a
recommendation system ("recsystem" for short) compatible with the Renewal
competition platform, as well as documentation for the sample implementation
in Python which can be extended to implement your own recsystem with a
minimal amount of boilerplate code.

.. contents:: Table of Contents
    :local:
    :depth: 3


Introduction
============

.. mdinclude:: ../README.md
    :start-line: 9


Full Documentation
==================

.. toctree::
    :maxdepth: 2

    quickstart
    interface
    recsystem
    backend
    api
    primer


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
