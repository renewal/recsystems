"""
Executable entrypoint for the ``renewal_recsystem`` package.

Use this to run the basic recsystem like::

    $ python -m renewal_recsystem

"""


import os.path as pth
import sys

from .basic import BasicRecsystem


BasicRecsystem.main(
    auto_envvar_prefix=BasicRecsystem.ENVVAR_PREFIX,
    prog_name=f'{pth.basename(sys.executable)} -m {__package__}'
)
