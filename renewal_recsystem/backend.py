"""
Documents the JSON-RPC interface to the Renewal backend.

This module contains just stub functions for documenting the API.  Example
implementations can be found in
`~renewal_recsystem.baseline.BaselineRecsystem`.
"""


RECOMMEND_DEFAULT_MAX_ARTICLES = 30
"""Max number of recommendations to return by default."""

RECOMMEND_DEFAULT_MIN_ARTICLES = 30
"""Mininum number of recommendations to return by default."""


__all__ = ['ping', 'stats', 'recommend', 'new_article', 'article_interaction',
           'assigned_user', 'unassigned_user']


def ping() -> str:
    """
    A simple heartbest test sent from the backend to the recsystem on a regular
    basis, in order to check if it is still responding and how quickly it
    responds.

    Although the WebSocket protocol itself has a low-level ping check, this is
    useful for the backend to check on its own interval if your recsystem is
    still alive.  For now it should only return the string ``"pong"``.

    :returns: The string ``"pong"``.
    :rtype: str
    """


def stats() -> dict:
    """
    Return various statistics about the recsystem since starting up.

    Must return a mapping with the following keys:

    * ping_count: number of times the recsystem has responded to a ping
    * seen_articles: article_ids of new articles seen by the recsystem (via the
      `new_article` RPC call, not counting articles manually downloaded via the
      REST API)
    * assigned_users: user_ids of currently assigned users

    Additional stats may be included for your own testing purpose, but will
    currently be ignored by the backend.

    .. note::

        This is currently the only RPC method which is optional.  It is useful
        for testing and debugging, and is required only if using the
        :ref:`recsystem smoke test <recsystem-smoke-test>` to test your
        recsystem.

    If you base your recsystem implementation on
    `~renewal_recsystem.recsystem.RenewalRecsystem` this method is already
    provided for you.  Just make sure that if you override any other methods,
    e.g.  such as `new_article` that you call the base implementation with
    `super`, like::

        class MyRecsystem(RenewalRecsystem):
            def new_article(self, article):
                super().new_article(article)
                # .. the rest of your implementation ...

    :returns: `dict` in the format ``{"ping_count": int, "seen_articles":
     list[int], "assigned_users": list[str]}``
    :rtype: dict
    """


def recommend(user_id, max_articles=RECOMMEND_DEFAULT_MAX_ARTICLES,
              min_articles=RECOMMEND_DEFAULT_MIN_ARTICLES, since_id=None,
              max_id=None) -> list:
    """
    Return recommendations for the specified user and article ID range.

    Although ``max_articles`` and ``min_articles`` are optional parameters, in
    practice they will always be sent by the Renewal backend.  You should set
    its default to something like `RECOMMEND_DEFAULT_MAX_ARTICLES
    <renewal_recsystem.recsystem.RenewalRecsystem.RECOMMEND_DEFAULT_MAX_ARTICLES>`,
    and likewise for ``RECOMMEND_DEFAULT_MIN_ARTICLES``.  Your recsystem must
    not return more recommendations than specified in ``max_articles``, and if
    it returns fewer than ``min_articles`` those recommendations will not be
    sent to the user.

    If ``since_id`` is given, returned recommendations should have an
    ``article_id`` greater than ``since_id``.  Likewise, if ``max_id`` is
    given, each ``article_id`` should be less than ``max_id``.  However, these
    are merely intended as hints for recsystems to keep their recommendations
    within the same time period, and *may* be disregarded.

    The return value should be a list of ``article_id`` of articles to
    recommend to the user.  It should just be the ``article_id``, *not* the
    full :ref:`article <data-structure-article>` data structure.

    :param str user_id: The ``user_id`` identifying the user to return
     recommendations for.
    :param int max_articles: The maximum number of recommendations to return.
    :param int min_articles: The minimum number of recommendations to return.
    :param int since_id: Each returned ``article_id`` *should* be greater than
     this value.
    :param int max_id: Each returned ``article_id`` *should* be less than
     this value.
    :returns: `list` of `int` representing ``article_id`` to recommend to the
     user.  Any bogus ``article_id`` will be ignored by the backend.
    :rtype: `list`
    """


# Notifications


def new_article(article):
    """
    *(notification)* Called every time the Renewal backend has scraped a
    new article.

    This allows your recsystem to add the article to its own internal
    database of articles.  Sends the recsystem all the details of the new
    article.

    :param dict article: A `dict` in the :ref:`article format
     <data-structure-article>`.
    """


def article_interaction(interaction):
    """
    *(notification)* Called when a user interacts with an article.

    This is received for all users, including users not currently assigned to
    the recsystem.

    This allows maintaining the recsystem's own up-to-date metrics on
    interactions with each article in its local database of articles.

    Interactions currently include:

    * Likes
    * Dislikes
    * Bookmarks
    * Clicks (the user clicked on the article)

    with more to be added later.

    :param dict interactions: A `dict` in the :ref:`article interaction
     format<data-structure-article-interaction>`
    """


def assigned_user(user_id):
    """
    *(notification)* Called when the backend has assigned a new user to your
    recsystem.

    The recsystem will in general only receive recommendation requests for
    users it is actively assigned to (though it may be sent requests for
    unassigned users for test purposes).  However, the recsystem can use this
    to maintain a set of users for whom it should be actively processing data.

    :param str user_id: The ``user_id`` identifying the user assigned to
     the recsystem.
    """


def unassigned_user(user_id):
    """
    *(notification)* Called when the backend removes a user assignment from the
    recsystem.

    See also `assigned_user`.

    :param str user_id: The ``user_id`` identifying the user unassigned from
     the recsystem.
    """
