import logging
import os
import pickle
import time
from collections import defaultdict
from functools import partial
from typing import Any, List, Dict, Optional

import numpy as np
import pandas as pd

from ..types import ArticleId, UserId, User


log = logging.getLogger(__name__.split('.')[-1])


# Initialize, shutdown, and background tasks:
# This loads the user data from a pickle file named
# renewal_recsystem.baseline.keywords.pickle (if it exists) during
# initialization, and it also saves the state during shutdown, as well as
# periodically (in case of unclean shutdown/crash) so that we don't lose too
# much data.


STATE_FILENAME = __name__ + '.pickle'


def initialize(state: dict, users: Dict[UserId, User],
               articles: pd.DataFrame) -> Dict[str, Any]:
    if os.path.isfile(STATE_FILENAME):
        with open(STATE_FILENAME, 'rb') as fobj:
            state = pickle.load(fobj)

        log.info(f'loaded previous state from {STATE_FILENAME}')

    return state


def save_state(state: dict) -> None:
    # Pickle the state first so that in case there are any exceptions we don't
    # open the file for writing (which could erase its previous contents)
    pickled_state = pickle.dumps(state)

    with open(STATE_FILENAME, 'wb') as fobj:
        fobj.write(pickled_state)

    log.info(f'saved updated state to {STATE_FILENAME}')


def every_minute(state: dict, users: Dict[UserId, User],
                 articles: pd.DataFrame) -> None:
    save_state(state)


def shutdown(state: dict) -> None:
    save_state(state)


# TODO: Refine the type defitions for interaction and for format of the state
# updates this returns.
async def article_interaction(state: dict,
                              user: User,
                              articles: pd.DataFrame,
                              article_id: int,
                              interaction: dict) -> Optional[Dict[str, Any]]:
    user_words = state.get('user_words', {}).get(user.uid, defaultdict(int))
    article = articles.loc[article_id]

    score = 0
    if interaction.get('clicked'):
        score += 1
    if 'rating' in interaction:
        score += interaction['rating']

    for keyword in article['keywords']:
        if score != 0:
            # Avoid populating user_words with keywords of score=0
            user_words[keyword] += score

    if 'clicked' in interaction or 'rating' in interaction:
        log.info(f'updated keyword ratings for user {user.uid}:')
        for word in article['keywords']:
            log.info(f'    {word}: {user_words[word]}')

    return {'user_words': {user.uid: user_words}}


# TODO: Add type hints to utility functions.
def make_word_vectors(user_words, article_words):
    all_words = set(user_words) | set(article_words)
    user_vec = np.array([user_words.get(w, 0) for w in all_words])
    article_vec = np.array([
        user_words.get(w, 0) if w in article_words else 0
        for w in all_words])
    return user_vec, article_vec


def cosine_similarity(a, b):
    return a.dot(b) / (np.linalg.norm(a) * np.linalg.norm(b))


def softmax(a):
    exps = np.exp(a)
    return exps / exps.sum()


def score_article(user_words, article_words):
    user_vec, article_vec = make_word_vectors(user_words, article_words)
    user_vec = softmax(user_vec)
    article_vec = softmax(article_vec)
    return cosine_similarity(user_vec, article_vec)


def make_recommendations(state: dict, user: User,
                         articles: pd.DataFrame) -> pd.DataFrame:
    t0 = time.time()
    user_words = state.get('user_words', {}).get(user.uid, {})
    score_func = partial(score_article, user_words)
    # sort articles by their keyword scores
    articles = articles.sort_values(
        'keywords',
        key=lambda k: k.apply(score_func),  # type: ignore
        ascending=False)
    log.info(f'generating recommendations for user {user.uid} took '
             f'{time.time() - t0} seconds')
    return articles


def recommend(state: dict, user: User, articles: pd.DataFrame,
              min_articles: int, max_articles: int) -> List[ArticleId]:
    t0 = time.time()
    articles = make_recommendations(state, user, articles)

    # take the top N based on max_articles
    articles = articles.iloc[:max_articles]

    log.info(f'top 10 articles for user {user.uid}:')
    user_words = state.get('user_words', {}).get(user.uid, {})

    for article_id, article in articles.iloc[:10].iterrows():
        score = score_article(user_words, article.keywords)
        log.info(f'    (score: {score}) {article_id}: {article.title}')

    log.info(f'fetching recommendations for user {user.uid}; took '
             f'{time.time() - t0} seconds')
    return list(articles.index)
