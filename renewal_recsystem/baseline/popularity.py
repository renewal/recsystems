"""Simple baseline recsystem based on article popularity."""


from typing import List

import pandas as pd

from ..types import User


def popularity(m: dict) -> int:
    """
    Returns a measure of an article's popularity.

    The formula is ``max(clicks, 1) * ((likes - dislikes) or 1)``.

    You could replace this with a more sophisticated measure of popularity.
    """
    clicks = m.get('clicks', 0)
    likes = m.get('likes', 0)
    dislikes = m.get('dislikes', 0)

    return max(1, clicks) * ((likes - dislikes) or 1)


def recommend(state: dict, user: User, articles: pd.DataFrame,
              min_articles: int, max_articles: int) -> List[int]:
    """
    Returns recommendations prioritized by the article's popularity.

    See `popularity` for "popularity" is defined in this case.
    """

    # Drop articles that don't have a 'metrics' dict
    articles = articles.dropna(subset=['metrics'])

    # Sort articles by most to least popular according to the
    # `popularity` function applied to their metrics dicts.
    articles = articles.sort_values(
        'metrics',
        key=lambda m: m.apply(popularity),  # type: ignore
        ascending=False)
    # Take the top `max_articles` most popular
    articles = articles.iloc[:max_articles]
    return list(articles.index)
