"""Simple baseline recsystem that returns random articles."""


import random
from typing import List

import pandas as pd

from ..types import User


async def recommend(state: dict, user: User, articles: pd.DataFrame,
                    min_articles: int, max_articles: int) -> List[int]:
    """Returns random recommendations from the article range."""

    limit = min(max_articles, len(articles))
    sample = random.sample(range(len(articles)), limit)
    return list(articles.index[sample])
