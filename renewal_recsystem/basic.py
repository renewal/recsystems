"""
Implements the basic recsystem.

It provides a base implementation for a working recsystem which can easily be
extended.  See :doc:`interface` for more details.
"""

# Python standard library modules
import asyncio
import logging
import time
from collections import defaultdict
from contextlib import suppress
from typing import Any, Dict, List, IO, Optional, Tuple
from urllib.parse import urljoin

# Third-party modules
import aiohttp
import objclick as click  # type: ignore
import pandas as pd

# Local modules
from . import __version__
from .articles import ArticleCollection
from .hookmanager import HookManager, HookModule, required, method
from .recsystem import RenewalRecsystem
from .types import ArticleId, User, UserId
from .utils import FileOrToken


# Local types
RecommendationsCacheEntry = Tuple[List[ArticleId], float]
RecommendationsCache = Dict[UserId, RecommendationsCacheEntry]


class BasicRecsystemHooks(HookManager):
    def initialize(state: dict,  # type: ignore
                   users: Dict[UserId, User],
                   articles: pd.DataFrame) -> None:
        """
        Run when the recsystem is first initialized, after some initial
        articles and user states have been fetched.
        """

    def shutdown(state: dict) -> None:  # type: ignore
        """
        Run during (clean) shutdown (e.g. with Ctrl-C) of the recsystem,
        allowing additional shutdown tasks such as saving files, etc.
        """

    @required
    @method
    def recommend(state: dict,  # type: ignore
                  user: User,
                  articles: pd.DataFrame,
                  min_articles: int,
                  max_articles: int) -> List[int]:
        """Called when recommendations are requested for a user."""

    def article_interaction(state: dict,  # type: ignore
                            user: User,
                            articles: pd.DataFrame,
                            article_id: int,
                            interaction: dict) -> None:
        """Called when a user interacts with an article."""

    def background_(state: dict,  # type: ignore
                    users: Dict[UserId, User],
                    articles: pd.DataFrame) -> None: ...

    def every_(state: dict,  # type: ignore
               users: Dict[UserId, User],
               articles: pd.DataFrame) -> None: ...


class BasicRecsystem(RenewalRecsystem):
    NAME = 'basic'

    INITIAL_ARTICLES = 1000
    """Number of articles to initialize the in-memory article cache with."""

    MAX_ARTICLES = 10000
    """Maximum number of articles to keep cached."""

    RECOMMENDATIONS_CACHE_RATE = 1  # second
    """
    How frequently to check if any users need new recommendations cached.

    This rate does not take into account how long it takes to populate the
    cache, only the delay between loops.
    """

    RECOMMENDATIONS_CACHE_TTL = 5 * 60  # seconds
    """How long before a cached recommendations list for a user expires."""

    articles: ArticleCollection
    """Articles cache; initialized in `initialize`."""

    users: Dict[UserId, User]
    """
    Dictionary of users assigned to the recsystem including information about
    their article interactions.

    Maps each user ID of assigned users to an associated `.User` instance.
    """

    def __init__(self, hook_module: HookModule,
                 api_base_uri: Optional[str] = None,
                 token: Optional[str] = None,
                 log: Optional[logging.Logger] = None) -> None:
        super().__init__(api_base_uri=api_base_uri, token=token, log=log)
        self.users = {}
        self.hooks = BasicRecsystemHooks.from_module(hook_module, self.log)
        self._http_session: Optional[aiohttp.ClientSession] = None

        self._recommendations_cache: RecommendationsCache = {}
        self._cache_recommendations_task: asyncio.Task

    async def initialize(self) -> None:
        """
        Start-up tasks to perform before starting the main client loop.

        * Download a initial set of recent articles to work with.
        * Download the current set of users assigned to the recsystem.
        * Start user-provided background tasks.
        """

        self.log.info(
                f'initializing articles cache with {self.INITIAL_ARTICLES} '
                f'articles')

        self._http_session = aiohttp.ClientSession(headers=self.client_headers,
                                                   raise_for_status=True)

        articles = await self.fetch(
            'articles', params={'limit': self.INITIAL_ARTICLES})
        if articles is None:
            articles = []

        self.articles = ArticleCollection(articles, max_size=self.MAX_ARTICLES)
        self.log.debug(f'cached {len(self.articles)} articles')

        users = await self.fetch('user_assignments')
        if users:
            for user in users:
                uid = user['user_id']
                interactions = user.get('interactions', [])
                interactions = {inter.pop('article_id'): inter
                                for inter in interactions}
                self.users[uid] = User(uid)
                self.users[uid].interactions.update(interactions)
            self.log.debug(f'cached {len(self.users)} assigned users')

        # Set the assigned_users stat to the self.users set
        self.runtime_stats.assigned_users = set(self.users)

        # User-provided initialization hook
        await self.hooks.initialize(users=self.users,
                                    articles=self.get_article_selection())

        # Start up any user-provided background tasks; these provide callables
        # for the 'users' and 'articles' keyword arguments so that their
        # up-to-date values can be retrieved every time each hook is called.
        def get_users():
            return self.users

        hook_kwargs = {
            'users': get_users,
            'articles': self.get_article_selection
        }

        await self.hooks.start_background_hooks(**hook_kwargs)
        await self.hooks.start_periodic_hooks(**hook_kwargs)
        self._cache_recommendations_task = asyncio.create_task(
            self._cache_recommendations())

    # ********** RPC methods **********
    # WARNING: Don't forget to make these functions async even if they
    # don't use await, otherwise the async_dispatch gets confused.
    async def new_article(self, article: dict) -> None:
        """Called when a new article was made available from the backend."""

        await super().new_article(article)

        self.articles.push(article)
        self.log.debug(f'new article added to the collection: {article}')
        self.log.debug(f'article collection size: {len(self.articles)}')

    async def article_interaction(self, interaction: dict) -> None:
        """
        Called when a user interacts with an article--this is received for all
        users, including users not currently assigned to the recsystem.

        This allows maintaining the recsystem's own up-to-date metrics on
        interactions with each article in its local database of articles.

        Interactions currently include:

        * Recommends (the article was recommended and sent to the user)
        * Likes
        * Dislikes
        * Bookmarks
        * Clicks (the user clicked on the article)

        with more to be added later.
        """

        article_id = interaction.pop('article_id')
        user_id = interaction.pop('user_id')

        if article_id not in self.articles:
            self.log.warning(
                f'article {article_id} not in my collection for some reason; '
                f'fetching it from the backend')
            article = await self.fetch('articles', str(article_id))
            if article:
                self.articles.push(article)
                self.log.info(
                    f'successfully fetched article {article_id} and added '
                    f'it to my collection')
            else:
                self.log.error(
                    f'failed to fetch article {article_id} from the backend')
                return

        # Update our stored copy of the article's metrics based on this
        # interaction event
        metrics = self.articles[article_id].setdefault('metrics',
                                                       defaultdict(int))

        if 'rating' in interaction:
            rating = interaction['rating']
            # prev_rating *should* be included with ratings, but we give it a
            # default value of 0 just in case
            prev_rating = interaction.pop('prev_rating', 0)
            if prev_rating == -1:
                metrics['dislikes'] -= 1
            elif prev_rating == 1:
                metrics['likes'] -= 1

            if rating == -1:
                metrics['dislikes'] += 1
            elif rating == 1:
                metrics['likes'] += 1

        bool_metrics = [
            ('bookmarked', 'bookmarks'),
            ('clicked', 'clicks'),
            ('recommended', 'recommends')
        ]

        for bool_metric in bool_metrics:
            # Update counts of clicks, bookmarks, etc.
            action, metric = bool_metric
            if action in interaction:
                # Increase if the value of the action was True and decrease if
                # it was False
                # NOTE: in practice 'clicks' can never decrease, but bookmarks
                # can decrease
                metrics[metric] += 1 if interaction[action] else -1

        self.log.debug(f'updated metrics for article {article_id}: {metrics}')

        # NOTE: We could also track interactions for users not assigned to us
        # but the baseline recsystem currently just tracks assigned users.
        # Subclasses could override this behavior.
        if user_id not in self.users:
            return

        self.users[user_id].interactions[article_id].update(interaction)
        articles = self.get_article_selection()
        await self.hooks.article_interaction(user=self.users[user_id],
                                             articles=articles,
                                             article_id=article_id,
                                             interaction=interaction)
        self.log.debug(
            f'updated user {user_id} record for article {article_id}: '
            f'{self.users[user_id].interactions[article_id]}')

    async def recommend(self, *, user_id: UserId,
                        min_articles: Optional[int] = None,
                        max_articles: Optional[int] = None,
                        since_id: Optional[int] = None,
                        max_id: Optional[int] = None) -> List[int]:
        """
        Return recommendations for the specified user and article ID range.
        """

        if min_articles is None:
            min_articles = self.RECOMMEND_DEFAULT_MIN_ARTICLES

        if max_articles is None:
            max_articles = self.RECOMMEND_DEFAULT_MAX_ARTICLES

        # If any arguments are different from the defaults we don't use the
        # cache
        if (min_articles == self.RECOMMEND_DEFAULT_MIN_ARTICLES and
                max_articles == self.RECOMMEND_DEFAULT_MAX_ARTICLES and
                since_id is None or max_id is None):
            use_cache = True
        else:
            use_cache = False

        if use_cache and user_id in self._recommendations_cache:
            self.log.info(f'using pre-computed cached recommendations for '
                          f'user {user_id}')
            return self._recommendations_cache.pop(user_id)[0]

        return await self._recommend(user_id=user_id,
                                     min_articles=min_articles,
                                     max_articles=max_articles,
                                     since_id=since_id, max_id=max_id)

    async def _recommend(self, *, user_id: UserId,
                         min_articles: Optional[int] = None,
                         max_articles: Optional[int] = None,
                         since_id: Optional[int] = None,
                         max_id: Optional[int] = None) -> List[int]:
        """
        Internal implementation for generating recommendations.

        `BasicRecsystem.recommend` is just a front-end to this which passes
        through the recommendations cache.
        """

        if min_articles is None:
            min_articles = self.RECOMMEND_DEFAULT_MIN_ARTICLES

        if max_articles is None:
            max_articles = self.RECOMMEND_DEFAULT_MAX_ARTICLES

        user = self.users[user_id]
        # filter out articles the user has already been recommended
        articles = self.get_article_selection(since_id=since_id,
                                              max_id=max_id,
                                              omit=list(user.interactions))

        recommendations = await self.hooks.recommend(
            user=user, articles=articles, min_articles=min_articles,
            max_articles=max_articles)

        if not isinstance(recommendations, list):
            # User recsysystems *should* return a list, but if they return
            # something like a Pandas Series or numpy array or something else
            # list-like we can forgive that and convert it to a list for them.
            recommendations = list(recommendations)

        return recommendations

    def get_article_selection(self,
                              since_id: Optional[int] = None,
                              max_id: Optional[int] = None,
                              omit: Optional[List[int]] = None
                              ) -> pd.DataFrame:
        """
        Return a list of articles from ``self.articles`` given
        ``max_articles``, ``min_articles``, and optional ``since_id`` and
        ``max_id`` arguments.

        If given, articles with IDs in the ``omit`` list are filtered out as
        well.
        """

        articles = pd.DataFrame(self.articles)
        if 'article_id' not in articles or len(articles) == 0:
            # the articles collection, and hence the dataframe, are empty
            return articles

        articles = articles.set_index('article_id')

        if max_id is None:
            start = None
        else:
            start = max_id - 1

        if since_id is None:
            end = 0
        else:
            end = since_id + 1

        # Note: articles indexed in reverse order of article_id with highest
        # article_id first, so since_id is the lower bound and max_id the upper
        # bound
        articles = articles.loc[start:end]
        if omit:
            articles = articles.loc[articles.index.difference(omit)]

        return articles

    async def fetch(self, *parts: str, **kwargs: Any) -> Any:
        """Fetch a resource from the backend API."""

        if self._http_session is None:
            raise RuntimeError('initialize() must be called before fetch() '
                               'can be used')

        url = urljoin(self.api_base_uri, '/'.join(parts))

        try:
            async with self._http_session.get(url, **kwargs) as resp:
                return await resp.json()
        except Exception as exc:
            self.log.error(f'failed to fetch {url}: {exc}')
            return None

    async def assigned_user(self, user_id: UserId) -> None:
        """
        Called when the controller has assigned a new user to the recsystem.

        The recsystem will in general only receive recommendation requests for
        users it is actively assigned to (though it may be sent requests for
        unassigned users for test purposes).  However, the recsystem can use
        this to maintain a set of users for whom it should be actively
        processing data.
        """

        await super().assigned_user(user_id)
        self.users[user_id] = User(user_id)
        self.log.info(f'assigned user {user_id}')

    async def unassigned_user(self, user_id: UserId) -> None:
        """
        Called when the controller removes a user assignment from the
        recsystem.

        See also `assigned_user`.
        """

        await super().unassigned_user(user_id)

        try:
            del self.users[user_id]
        except KeyError:
            self.log.warning(
                f'user {user_id} was unassigned, but we did not have that '
                f'user in the set of assigned users in the first place')

        # Clean up any cached recommendations for that user
        try:
            del self._recommendations_cache[user_id]
        except KeyError:
            pass

        self.log.info(f'unassigned user {user_id}')

    async def shutdown(self) -> None:
        # Kill the recommendations task
        self._cache_recommendations_task.cancel()
        with suppress(asyncio.CancelledError):
            await self._cache_recommendations_task

        # Call the user's shutdown callback
        await self.hooks.shutdown()
        # Shut down background hooks
        await self.hooks.stop_hooks()
        if self._http_session is not None:
            await self._http_session.close()

    async def _cache_recommendations(self) -> None:
        """
        Task for generating cached recommendation lists.

        Every `.RECOMMENDATIONS_CACHE_RATE` seconds it loops over the list of
        assigned users and pre-computes a reclist for any user who doesn't have
        a cached reclist, or for whom the existing cached reclist is expired.
        """

        rate = self.RECOMMENDATIONS_CACHE_RATE
        ttl = self.RECOMMENDATIONS_CACHE_TTL

        while True:
            for user_id in list(self.users):
                cached = self._recommendations_cache.get(user_id)
                now = time.time()
                if cached is None or ((now - cached[1]) > ttl):
                    # Get recommendations using the default settings
                    reclist = await self._recommend(user_id=user_id)
                    self._recommendations_cache[user_id] = (reclist, now)
                    self.log.info(f'cached pre-computed recommendations for '
                                  f'user {user_id}')

            await asyncio.sleep(rate)

    @click.classcommand()
    @click.option('-a', '--api-base-uri',
                  default=RenewalRecsystem.RENEWAL_API_BASE_URI,
                  help='URI for the Renewal HTTP API')
    @click.option('-t', '--token', required=True, type=FileOrToken(),
                  help='authentication token for the recsystem; if a valid '
                       'filename is given the token is read from a file '
                       'instead')
    @click.option('--log-level', default='INFO',
                  type=click.Choice(['DEBUG', 'INFO', 'WARNING', 'ERROR'],
                                    case_sensitive=False),
                  help='minimum log level to output')
    @click.version_option(__version__, message='%(version)s')
    @click.argument('module')
    def main(cls, api_base_uri: str, token: IO, log_level: str, module: str,
             **kwargs):
        """Recsystem command line interface."""

        log_name = module

        return super().main.callback(
            hook_module=module, api_base_uri=api_base_uri, token=token,
            log_level=log_level, log_name=log_name, **kwargs)
