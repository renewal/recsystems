import asyncio
import importlib.machinery
import inspect
import logging
import logging.handlers
import multiprocessing as mp
import os.path as pth
import re
import runpy
import signal
import sys
import threading
import types
import warnings
from concurrent.futures import ProcessPoolExecutor
from functools import partial
from typing import (Any, Callable, Coroutine, Dict, Iterable, Optional,
                    Set, Union, cast)

from .utils import dict_merge


def required(func):
    """
    Decorator for use with `HookManager` subclasses for marking a hook as
    required.
    """

    func._hook_required = True
    return func


def method(func):
    """
    Decorator for use with `HookManager` subclasses for marking a hook as a
    method.

    Method hooks have a return value other than a state update, or optionally
    can return a ``(return_value, state_update)`` tuple.  In order to avoid
    ambiguity, thus, ``return_value`` must not be a 2-tuple.
    """

    func._hook_method = True
    return func


HookModule = Union[str, types.ModuleType]
HookFunc = Callable[..., Any]


class HookManager:
    _hook_stubs: Dict[str, HookFunc] = {}
    _forbidden_hook_names = ['start_background_hooks', 'start_periodic_hooks',
                             'stop_hooks']
    _module: Optional[HookModule] = None
    _logging_thread: Optional[threading.Thread] = None
    _log_queue: mp.Queue

    def __init__(self, hooks: Dict[str, Any],
                 name: Optional[str] = None,
                 log: Optional[logging.Logger] = None) -> None:
        if name is None:
            name = self.__class__.__name__

        self._name = name

        if log is None:
            self._log = logging.getLogger(self.__class__.__name__)
        else:
            self._log = log

        self._state: Dict[Any, Any] = {}
        self._init_hooks(hooks)
        self._executor: Optional[ProcessPoolExecutor] = None
        self._tasks: Set[Union[asyncio.Task, asyncio.Future]] = set()

    def __init_subclass__(cls) -> None:
        cls._hook_stubs = cls._hook_stubs.copy()
        # All functions in the class body that are not _hidden or __special__
        # are considered hook stubs
        for name, value in cls.__dict__.items():
            if name in cls._forbidden_hook_names:
                raise RuntimeError(f'{name} is not allowed as a hook name')
            if not name.startswith('_') and inspect.isfunction(value):
                cls._hook_stubs[name] = value

        if 'background_' not in cls._hook_stubs:
            def background_(state: dict):
                pass

            cls._hook_stubs['background_'] = background_

        if 'every_' not in cls._hook_stubs:
            def every_(state: dict):
                pass

            cls._hook_stubs['every_'] = every_

        # Delete the stubs from the class namespace
        for hook_name in cls._hook_stubs:
            try:
                delattr(cls, hook_name)
            except AttributeError:
                pass

    @classmethod
    def from_module(cls, module: HookModule,
                    log: Optional[logging.Logger]) -> 'HookManager':
        """Load the hooks from a Python module."""

        if isinstance(module, str):
            hooks = cls._load_module(module)
            module_name = module
        else:
            hooks = module.__dict__
            module_name = module.__name__

        self = cls(hooks, name=module_name, log=log)
        self._module = module
        return self

    def __getattr__(self, attr):
        """
        For registered hook functions, return a partial application of the hook
        function that can be called with any keyword arguments specific to that
        function.

        The ``self._state`` is also automatically applied as the first
        argument.
        """

        if attr in self._hooks:
            return partial(self._run_hook, attr)
        elif attr in self._hook_stubs:
            # If it is a proper hook function defined by the interface, but is
            # not implemented by the contestant, we just return a no-op.
            #
            # If it was a required method that wasn't implemented this would
            # have already been caught at class definition time (in
            # __init_subclass__)
            async def noop(*args, **kwargs):
                pass

            return noop

        raise AttributeError(attr)

    async def stop_hooks(self) -> None:
        """
        Perform any shutdown-time steps for the hook manager.

        * Shuts down all background tasks.
        * Shuts down the process pool executor.
        * Shuts down additional processes handling background tasks.
        """

        for task in self._tasks:
            task.cancel()

        await asyncio.gather(*self._tasks, return_exceptions=True)

        if self._executor is not None:
            self._executor.shutdown()

        if self._logging_thread is not None:
            self._log_queue.put(None)
            self._logging_thread.join()

    async def start_background_hooks(self, **kwargs: Any) -> None:
        """
        Start running all background hooks, passing them the given keyword
        arguments.

        If any of the keyword arguments are callables (taking no arguments)
        those callables are *called* to retreive the keyword value each time
        each hook is run.

        The hooks are automatically passed the current state dict.
        """

        hooks = [hook_name for hook_name, period in self._background.items()
                 if period == 0]
        await self._start_hooks(hooks, **kwargs)

    async def start_periodic_hooks(self, **kwargs: Any) -> None:
        """
        Start running all background hooks, passing them the given keyword
        arguments.

        If any of the keyword arguments are callables (taking no arguments)
        those callables are *called* to retreive the keyword value each time
        each hook is run.

        The hooks are automatically passed the current state dict.
        """

        hooks = [hook_name for hook_name, period in self._background.items()
                 if period > 0]
        await self._start_hooks(hooks, **kwargs)

    async def _start_hooks(self, hook_names: Iterable[str],
                           **kwargs: Any) -> None:
        """Helper for start_background/periodic hooks."""

        tasks = [asyncio.create_task(self._run_hook(hook_name, **kwargs))
                 for hook_name in hook_names]
        self._tasks.update(tasks)

    def _init_hooks(self, hooks: Dict[str, HookFunc]) -> None:
        self._hooks = {}
        self._methods = set()

        # Contains both background_ and every_ hooks, mapping the hook name to
        # its period; for background_ tasks the period is simply 0
        self._background = {}

        for hook_name, hook_func in hooks.items():
            if hook_name.startswith('every_'):
                period = self._parse_periodic_hook(hook_name)
                if period is not None:
                    self._background[hook_name] = period
                    stub = self._hook_stubs['every_']
            elif hook_name.startswith('background_'):
                self._background[hook_name] = 0
                stub = self._hook_stubs['background_']
            elif hook_name in self._hook_stubs:
                stub = self._hook_stubs[hook_name]
                if getattr(stub, '_hook_method', False):
                    self._methods.add(hook_name)
            else:
                # else, not a known hook function, so we ignore it
                continue

            # Validate the method's signature against its stub signature;
            # this should help competitors ensure that they at least have the
            # right signatures for their hooks
            self._validate_signature(hook_name, hook_func, stub)
            self._hooks[hook_name] = hook_func

        # Make sure all required hooks are registered
        for hook_name, hook_stub in self._hook_stubs.items():
            if (getattr(hook_stub, '_hook_required', False) and
                    hook_name not in self._hooks):
                raise RuntimeError(
                    f'a hook function with the name and signature '
                    f'{hook_name}{inspect.signature(hook_stub)} is required '
                    f'for your recsystem')

    def _update_state(self, state_update):
        """
        Update the state dict.

        Currently just replaces the previous state dict with the result of
        ``dict_merge(self._state, state_update)``.
        """

        self._state = dict_merge(self._state, state_update)

    async def _run_hook(self, hook_name: str, **kwargs: Any):
        hook_func = self._hooks[hook_name]
        if hook_name in self._background:
            period = self._background[hook_name]
            coro = self._wrap_background_hook(hook_func, period, **kwargs)
        else:
            coro = self._wrap_hook(hook_func, **kwargs)

        res = await coro

        if hook_name in self._methods:
            if isinstance(res, tuple) and len(res) == 2:
                res, state_update = res
            else:
                state_update = None
        else:
            state_update = res
            res = None

        if state_update is not None:
            self._update_state(state_update)

        return res

    def _wrap_background_hook(self, hook_func: HookFunc, period: int,
                              **kwargs: Any) -> Coroutine:
        """
        Wrap the given hook function so that it is schedule to run every
        ``period`` seconds as specified by the function name.
        """

        async def wrapper() -> None:
            self._log.info(f'running {hook_func.__name__} every {period} '
                           f'seconds')
            while True:
                coro = self._wrap_hook(hook_func, **kwargs)
                try:
                    state_update = await coro
                except asyncio.CancelledError:
                    raise
                except Exception as exc:
                    # This is so we just log the original traceback from the
                    # wrapped hook function when raised from a
                    # concurrent.futures excutor
                    if exc.__cause__ is not None:
                        exc = cast(Exception, exc.__cause__)

                    exc_info = (type(exc), exc, exc.__traceback__)
                    self._log.exception(
                        f'an exception occurred in background hook function '
                        f'{hook_func.__name__}:',
                        exc_info=exc_info)
                    state_update = None

                    if period < 1:
                        # Prevent totally spamming an exception for hooks that
                        # have a short period
                        await asyncio.sleep(1)
                        continue

                if state_update is not None:
                    self._update_state(state_update)
                await asyncio.sleep(period)

        return wrapper()

    def _wrap_hook(self, hook_func: HookFunc, **kwargs: Any) -> Coroutine:
        """
        Wrap the given hook function in a coroutine.

        If the function is already a coroutine function just call it, otherwise
        wrap it in a ``run_in_executor`` call.
        """

        # Implement argument value callables; if the value is a
        # callable with zero arguments it is called before passing to
        # the function
        kwargs = {k: v() if callable(v) else v for k, v in kwargs.items()}
        func = partial(hook_func, state=self._state, **kwargs)
        if inspect.iscoroutinefunction(hook_func):
            coro = func()
        else:
            loop = asyncio.get_event_loop()
            if self._executor is None:
                # start the logging thread / queue
                self._log_queue = mp.Queue()
                self._logging_thread = threading.Thread(
                    target=self._logging_thread_worker,
                    args=(self._log_queue,))
                self._logging_thread.start()
                self._executor = ProcessPoolExecutor(
                    initializer=self._init_worker,
                    initargs=(self._module, self._log_queue))
            coro = loop.run_in_executor(self._executor, func)
        return coro

    @classmethod
    def _init_worker(cls, module=None, log_queue=None):
        """
        Used to initialize process pool workers.

        They should ignore SIGINT and instead let the parent process clean up.
        """

        signal.signal(signal.SIGINT, signal.SIG_IGN)
        if log_queue is not None:
            # clear existing root logger handlers (e.g. configured via
            # logging.basicConfigure); this isn't flexible enough at the moment
            # to disable all other possible logging cases...
            root = logging.getLogger()
            for handler in root.handlers[:]:
                root.removeHandler(handler)
                handler.close()

            handler = logging.handlers.QueueHandler(log_queue)
            # log all messages; the logger in the main process will handle
            # level filtering accordingly to how it's configured
            root.setLevel(logging.DEBUG)
            root.addHandler(handler)

        if isinstance(module, str):
            cls._load_module(module)

    @staticmethod
    def _logging_thread_worker(log_queue):
        """Handle log records sent from the worker processes."""

        while True:
            record = log_queue.get()
            if record is None:
                # poison pill to stop the thread
                break

            logger = logging.getLogger(record.name)
            logger.handle(record)

    _periodic_hook_re = re.compile(
        r'^every_(?:(?P<n>\d+)_)?(?P<unit>second|minute|hour|day)s?')

    def _parse_periodic_hook(self, hook_name: str):
        """
        Parse the name of an ``every_<duration>`` hook and return its actual
        duration in seconds.
        """

        units = {
            'second': 1,
            'minute': 60,
            'hour': 60 * 60,
            'day': 24 * 60 * 60
        }

        m = self._periodic_hook_re.match(hook_name)
        if not m:
            warnings.warn(
                f'hook function {hook_name} is not in the correct format '
                f'for every_ functions; must be '
                f'every_<second|minute|hour|day> or '
                f'every_<n>_<seconds|minutes|hours|days> where <n> is an '
                f'integer')
            return

        n = int(m.groupdict()['n'] or 1)
        unit = m.groupdict()['unit']
        return units[unit] * n

    @staticmethod
    def _validate_signature(hook_name: str, hook_func: HookFunc,
                            stub: HookFunc) -> None:
        """
        Validate the signature of a user-provided hook function against the
        interface defined by its stub.

        For the moment, annotations don't have to match, so annotations
        are stripped off before comparing.
        """

        def strip_annotations(sig):
            params = []
            for param in sig.parameters.values():
                params.append(param.replace(annotation=sig.empty))

            return sig.replace(parameters=params,
                               return_annotation=sig.empty)

        hook_sig = strip_annotations(inspect.signature(hook_func))
        stub_sig = strip_annotations(inspect.signature(stub))
        if hook_sig != stub_sig:
            raise RuntimeError(
                f'hook function {hook_name} must match the signature '
                f'{hook_name}({stub_sig})')

    @staticmethod
    def _load_module(module: str) -> dict:
        all_suffixes = importlib.machinery.all_suffixes()
        base, ext = pth.splitext(module)
        if pth.isfile(module) and ext in all_suffixes:
            # See https://github.com/python/typeshed/issues/4964
            modname = pth.basename(module)
            globs = runpy.run_path(module, run_name=modname)  # type: ignore
            # Hack to allow functions loaded from the file to be pickled (e.g.
            # for multiprocessing)
            mod = types.ModuleType(modname, doc=globs.get('__doc__'))
            mod.__dict__.update(globs)
            sys.modules[modname] = mod
        else:
            mod = importlib.import_module(module)
            globs = mod.__dict__

        return globs
