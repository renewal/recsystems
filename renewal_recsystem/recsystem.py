"""
Implements a base class to use for Renewal recsystems.

This provides less built-in functionality than
`~renewal_recsystem.baseline.BasicRecsystem`.  With the exception of the
`~renewal_recsystem.recsystem.RenewalRecsystem.ping` and
`~renewal_recsystem.recsystem.RenewalRecsystem.stats` methods, none of the
other required RPC methods are implemented, and are left to the user to
implement.

This can be used as a base class for a recsystem instead of
`~renewal_recsystem.baseline.BasicRecsystem` if you want to implement more
parts of the recsystem from scratch (e.g. use a more sophisticated database for
storing articles).
"""


import abc
import logging
import sys
from dataclasses import asdict, dataclass, field
from typing import Any, Dict, IO, List, Optional, Set
from urllib.parse import urlparse, urlunparse, urljoin

# Third-party modules
import coloredlogs  # type: ignore
import objclick as click  # type: ignore

# Local modules
from .backend import *  # noqa: F403
from .backend import (__all__ as RPC_METHODS, RECOMMEND_DEFAULT_MAX_ARTICLES,
                      RECOMMEND_DEFAULT_MIN_ARTICLES)
from .server import JSONRPCServerWebsocketClient
from .utils import FileOrToken, inherit_docstring


@dataclass
class RecsystemStats:
    """
    Runtime statistics of the recsystem; see the `RenewalRecsystem.stats` RPC
    method.
    """

    ping_count: int = 0
    seen_articles: Set[int] = field(default_factory=set)
    assigned_users: Set[str] = field(default_factory=set)

    def to_json(self) -> Dict[str, Any]:
        # The default stats contain a number of sets but these aren't
        # JSON-serializable by default; we could use a custom JSON serializer
        # but jsonrpcserver doesn't make it all that easy to do that, so just
        # perform the necessary conversions here.
        stats = asdict(self)
        for k, v in list(stats.items()):
            if isinstance(v, set):
                stats[k] = list(v)

        return stats


class RenewalRecsystem(JSONRPCServerWebsocketClient, metaclass=abc.ABCMeta):
    @property
    @abc.abstractmethod
    def NAME(self):
        """
        Display name for this recsystem.

        Must be assigned by subclasses.
        """

    ENVVAR_PREFIX: str = 'RENEWAL'
    """
    Prefix for environment variables that can be used to pass arguments to
    the recsystem's CLI.

    E.g. If the CLI has an argument named ``token`` this can be set either
    by passing ``--token=<token>`` at the command line, or by setting the
    environment variable ``RENEWAL_TOKEN=<token>``.
    """

    RENEWAL_API_BASE_URI: str = 'https://api.renewal-research.com/v1/'
    """Default URI for the Renewal API."""

    RPC_METHODS = RPC_METHODS  # specified in renewal_recsystem.backend
    """List of method names that implement RPC methods."""

    RECOMMEND_DEFAULT_MAX_ARTICLES = RECOMMEND_DEFAULT_MAX_ARTICLES
    """Max number of recommendations to return by default."""

    RECOMMEND_DEFAULT_MIN_ARTICLES = RECOMMEND_DEFAULT_MIN_ARTICLES
    """Min number of recommendations to return by default."""

    EVENT_STREAM_ENDPOINT: str = '/event_stream'
    """This is the endpoint on the Renewal API to connect to the websocket."""

    def __init__(self, api_base_uri: Optional[str] = None,
                 token: Optional[str] = None, path: Optional[str] = None,
                 log: Optional[logging.Logger] = None):
        if api_base_uri is not None:
            self.api_base_uri = api_base_uri
        else:
            # set it to the default
            self.api_base_uri = self.RENEWAL_API_BASE_URI

        if self.api_base_uri[-1] != '/':
            # Add trailing slash to make it easier to join URL fragments
            # with urljoin()
            self.api_base_uri += '/'

        base_uri = urlparse(self.api_base_uri)
        ws_proto = 'wss' if base_uri.scheme == 'https' else 'ws'
        ws_base_uri = urlunparse((ws_proto,) + base_uri[1:])
        websocket_uri = urljoin(ws_base_uri,
                                self.EVENT_STREAM_ENDPOINT.lstrip('/'))

        self.token = token
        self.runtime_stats = RecsystemStats()
        super().__init__(websocket_uri, path=path, log=log)

    @property
    def client_headers(self) -> Dict[str, str]:
        """
        Returns the headers passed on HTTP(S) requests to the Renewal API.
        """

        if self.token is None:
            self.log.warning(
                f'no authentication token provided; most requests to the '
                f'backend with be returned unauthorize except when testing '
                f'against a development server')
            return {}
        else:
            return {'Authorization': 'Bearer ' + self.token}

    @abc.abstractmethod
    async def initialize(self) -> None:
        """
        Start-up tasks to perform before starting the main client loop.

        * Download a initial set of recent articles to work with.
        * Download the current set of users assigned to the recsystem.

        Must be implemented by subclasses.
        """

    # ********** RPC methods **********
    # WARNING: Don't forget to make these functions async even if they
    # don't use await, otherwise the async_dispatch gets confused.
    @inherit_docstring(ping)  # noqa: F405
    async def ping(self) -> str:
        self.runtime_stats.ping_count += 1
        return 'pong'

    @inherit_docstring(stats)  # noqa: F405
    async def stats(self) -> Dict[str, Any]:
        return self.runtime_stats.to_json()

    @abc.abstractmethod
    @inherit_docstring(recommend)  # noqa: F405
    async def recommend(
            self, user_id: str,
            max_articles: int = RECOMMEND_DEFAULT_MAX_ARTICLES,
            min_articles: int = RECOMMEND_DEFAULT_MIN_ARTICLES,
            since_id: Optional[int] = None,
            max_id: Optional[int] = None) -> List[int]:
        pass

    @abc.abstractmethod
    @inherit_docstring(new_article)  # noqa: F405
    async def new_article(self, article: dict) -> None:
        # Update the seen_articles stat
        self.runtime_stats.seen_articles.add(article['article_id'])

    @abc.abstractmethod
    @inherit_docstring(article_interaction)  # noqa: F405
    async def article_interaction(self, interaction: dict) -> None:
        pass

    @abc.abstractmethod
    @inherit_docstring(assigned_user)  # noqa: F405
    async def assigned_user(self, user_id: str) -> None:
        self.runtime_stats.assigned_users.add(user_id)

    @abc.abstractmethod
    @inherit_docstring(unassigned_user)  # noqa: F405
    async def unassigned_user(self, user_id: str) -> None:
        try:
            self.runtime_stats.assigned_users.remove(user_id)
        except KeyError:
            pass

    def run(self) -> None:
        """
        Run the main event loop for the recommendation system.

        Starts by calling `initialize`, and once that is complete starts up
        the websocket connection and runs forever.
        """

        self.log.info(
                f'starting up {self.NAME} recsystem on {self.api_base_uri}')
        super().run()

    @click.classcommand()
    @click.option('-a', '--api-base-uri', default=RENEWAL_API_BASE_URI,
                  help='URI for the Renewal HTTP API')
    @click.option('-t', '--token', required=True, type=FileOrToken(),
                  help='authentication token for the recsystem; if a valid '
                       'filename is given the token is read from a file '
                       'instead')
    @click.option('--log-level', default='INFO',
                  type=click.Choice(['DEBUG', 'INFO', 'WARNING', 'ERROR'],
                                    case_sensitive=False),
                  help='minimum log level to output')
    def main(cls, api_base_uri: str, token: IO, log_level: str,
             log_name: Optional[str] = None, **kwargs) -> None:
        """
        Recsystem command line interface.

        The arguments ``api_base_uri``, ``token``, and ``log_level`` are
        passed by `click` via the command line interface.  Additional
        ``kwargs`` may be passed by subclasses that add additional options to
        the CLI, and their values are passed to the constructor of the
        subclass.

        .. note::

            When overriding the `main` function in a subclass it is necessary
            to copy all of the decorators above it, including the
            ``@click.classcommand()`` and all ``@click.option()`` decorators
            in addition to any new options.  This may be improved in a future
            version.
        """

        # Initialize logging
        logging.basicConfig(level=log_level)
        log = logging.getLogger(log_name or cls.NAME)
        log.setLevel(log_level)
        coloredlogs.install(level=log_level, logger=log)

        # Set the default log level for jsonrpcserver higher; currently it
        # produces more noise than necessary since we have our own logging of
        # RPC calls
        logging.getLogger('jsonrpcserver').setLevel(logging.WARNING)

        # Log all uncaught exceptions
        sys.excepthook = lambda *exc_info: log.exception(
                'an uncaught exception occurred',
                exc_info=exc_info)  # type: ignore

        token = token.read().strip()
        recsystem = cls(api_base_uri=api_base_uri, token=token,  # type: ignore
                        log=log, **kwargs)
        # recsystem.run() should never return, but we still return a value from
        # it for testing purposes.
        return recsystem.run()
