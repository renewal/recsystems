"""Provides the base server code for implementing a recsystem."""

# -*- encoding: utf-8 -*-
import abc
import asyncio
import logging
import time
from asyncio import Queue
from functools import partial
from typing import Dict, List, Optional, Union

# Third-party modules
import websockets
from jsonrpcserver import async_dispatch as dispatch
from jsonrpcserver.methods import Methods
from websockets import WebSocketClientProtocol

# Local modules
from .utils import format_rpc_call, shutdown


Uri = str


class JSONRPCServerWebsocketClient(metaclass=abc.ABCMeta):
    """
    Server code for running a websocket server that accepts and responds to
    JSON-RPC requests made by a remote server that is connected to via a
    websocket.

    It can handle multiple concurrent requests and response to them out of
    order in which they were received.

    To be clear, this is a *client* of the websocket protocol (it connects to
    a web server that hosts websocket connections) but is a *server* of the
    JSON-RPC protocol (it response to RPC requests sent over the websocket
    connection).

    On the other end this requires a websocket server which acts as the
    JSON-RPC client; it must also be able to handle multiple concurrent
    RPC calls and manage out-of-order responses.  See
    `~renewal_recsystem.utils.testing.WebSocketsMultiClient` for a sample
    implementation.

    This class is intended to be subclassed:

    * The names of RPC methods implemented by this server must be provided in
      the `RPC_METHODS` list, and the subclass should have methods of the same
      names.

    * An optional `initialize` method may be implemented to perform any tasks
      prior to connecting to the websocket.

    * An optional `client_headers` property/attribute may be used to pass
      additional HTTP headers (e.g. for authorization) when making the
      websocket connection.

    Parameters
    ----------
    uri : str, optional
        Full URI of the websocket to connect to (defaults to
        ``'ws://localhost/'``).

    path : str, optional
        Path to a UNIX socket when the websocket is served over a UNIX socket
        instead of TCP; mostly useful for testing/debugging.

    log : `logging.Logger`, optiona
        An optional `logging.Logger` to use as the server log; otherwise it
        creates its own log with the default handler.

    Examples
    --------

    Here we demonstrate creation of a simple `JSONRPCServerWebsocketClient`
    with two JSON-RPC methods ``square``, and ``greeting``.  To make the
    example more interesting, the ``square`` takes as long in seconds as the
    number it's squaring.  That should allow us to demonstrate that RPCs do not
    necessarily need to complete in the order they are called by the client.

    >>> import asyncio
    >>> from renewal_recsystem.server import JSONRPCServerWebsocketClient
    >>> class MyRPCServer(JSONRPCServerWebsocketClient):
    ...     RPC_METHODS = ['square', 'greeting']
    ...     # Ensures the server exits after losing connection
    ...     RETRY_RATE = None
    ...     greetings = []
    ...
    ...     async def square(self, x):
    ...         await asyncio.sleep(x)
    ...         return x**2
    ...
    ...     async def greeting(self, greeting):
    ...         # log the greeting; we'll print it later
    ...         self.greetings.append(f'received greeting: {greeting}')
    ...

    Now we create a test client that will make some calls to our RPC server.
    We create a websocket server that uses the websocket connection as a
    JSON-RPC client to make a few calls to our RPC server.

    Since we start with ``square(5)`` it will take the longest to complete
    even though it's called first, and so on.  All the results are still
    returned in the correct order and shouldn't take longer than the longest
    call to complete.  We also enable logging on the RPC client so we can
    see the requests and responses in the order they are sent and received.

    >>> import os.path as pth
    >>> import time
    >>> from renewal_recsystem.utils.testing import (
    ...     websocket_test_server_thread, WebSocketsMultiClient as WSClient)
    >>> async def handler(ws, path):
    ...     async with WSClient(ws, basic_logging=True) as rpc_client:
    ...         start_time = time.time()
    ...         responses = await asyncio.gather(
    ...             rpc_client.square(5),
    ...             rpc_client.square(3),
    ...             rpc_client.square(2),
    ...             rpc_client.square(1)
    ...         )
    ...         end_time = time.time()
    ...         print(f'result: {[r.data.result for r in responses]}')
    ...         print(f'took about {int(end_time - start_time)} seconds')
    ...         # Test a notify method as well
    ...         await rpc_client.notify('greeting', greeting='オス！')
    ...

    Create a temporary directory for the UNIX socket that we'll use for
    testing, then start the RPC client in a thread and wait until it's ready:

    >>> tmpdir_factory = getfixture('tmpdir_factory')  # pytest specific
    >>> sockpath = pth.join(tmpdir_factory.mktemp('unix'), 'ws.sock')
    >>> client_thread = websocket_test_server_thread(handler, path=sockpath)

    Create a new event loop: This is only strictly necessary for running the
    doctests, since the current loop will be closed when the server exits.

    >>> loop = getfixture('event_loop')  # pytest specific

    Finally run the RPC server.  When the client completes all its requests it
    will exit and the RPC server will disconnect from the websocket.

    >>> rpc_server = MyRPCServer(path=sockpath)
    >>> rpc_server.run()
    --> {"jsonrpc": "2.0", "method": "square", "params": [5], "id": 1}
    --> {"jsonrpc": "2.0", "method": "square", "params": [3], "id": 2}
    --> {"jsonrpc": "2.0", "method": "square", "params": [2], "id": 3}
    --> {"jsonrpc": "2.0", "method": "square", "params": [1], "id": 4}
    <-- {"jsonrpc": "2.0", "result": 1, "id": 4}
    <-- {"jsonrpc": "2.0", "result": 4, "id": 3}
    <-- {"jsonrpc": "2.0", "result": 9, "id": 2}
    <-- {"jsonrpc": "2.0", "result": 25, "id": 1}
    result: [25, 9, 4, 1]
    took about 5 seconds
    --> {"jsonrpc": "2.0", "method": "greeting", "params": {"greeting":
    "\\u30aa\\u30b9\\uff01"}}
    <--
    >>> for greeting in rpc_server.greetings: print(greeting)
    received greeting: オス！

    A bit of cleanup--the thread should have completed by now; if not there
    is a mistake in the code.  So we give the thread 5 seconds to join:

    >>> client_thread.join(timeout=5)
    >>> client_thread.is_alive()
    False

    """

    NAME: Optional[str] = None
    """
    Optional display name for instances of this server; otherwise the class
    name is used.
    """

    @property
    @abc.abstractmethod
    def RPC_METHODS(self) -> List[str]:
        """List of method names that implement RPC methods."""

    RETRY_RATE = 5  # seconds
    """
    Rate in seconds to retry connecting to the API when connection fails or
    is dropped.
    """

    _initialized = False

    def __init__(self, websocket_uri: Uri = 'ws://localhost/',
                 path: Optional[str] = None,
                 log: Optional[logging.Logger] = None) -> None:
        self.websocket_uri = websocket_uri
        self.path = path

        if log is None:
            self.log = logging.getLogger(self.NAME or self.__class__.__name__)
        else:
            self.log = log

        methods = []
        for method_name in self.RPC_METHODS:
            method = getattr(self, method_name, None)
            if not method:
                # TODO: Maybe also validate the signature of each method
                self.log.warning(
                    f'required RPC method {method_name} not implemented')
            methods.append(method)

        self.methods = Methods(*methods)

    @property
    def client_headers(self) -> Dict[str, str]:
        """
        Returns the HTTP headers to be passed when opening the websocket
        connection.
        """

        return {}

    async def initialize_if_needed(self) -> None:
        if self._initialized:
            return

        await self.initialize()
        self._initialized = True

    async def initialize(self) -> None:
        """
        Start-up tasks to perform before starting the main websocket client
        loop.
        """

        pass

    async def shutdown(self) -> None:
        """
        Shutdown-up tasks to perform before stopping the main websocket client
        loop.
        """

        pass

    async def request_loop(self) -> None:
        """
        Main loop of the recsystem application.

        Connects to the event stream websocket and starts a loop to receive and
        handle events from the backend.
        """

        self.log.info(
            f'initializing websocket connection to {self.websocket_uri}')

        if self.path is None:
            websocket_connect = partial(websockets.connect, self.websocket_uri,
                                        extra_headers=self.client_headers)
        else:
            websocket_connect = partial(websockets.unix_connect, self.path)

        async with websocket_connect() as websocket:
            self.log.info(f'listening to websocket for RPC requests...')
            # Incoming RPC requests are added to this queue, and their results
            # are popped off the queue and sent; the queue is used as a means
            # of serializing responses, otherwise we could have multiple
            # coroutines concurrently trying to write to the same websocket
            queue: Queue = asyncio.Queue()

            # Start the incoming and outgoing message handlers; a slight
            # variant of this pattern:
            # https://websockets.readthedocs.io/en/stable/intro.html#both
            await self.multiplex_tasks(self.handle_incoming(websocket, queue),
                                       self.handle_outgoing(websocket, queue))

    @staticmethod
    async def multiplex_tasks(*tasks) -> None:
        """
        Run multiple coroutines simultaneously as tasks, exiting as soon as any
        one of them raises an exception.

        The exception from the coroutine is then re-raised.
        """

        done, pending = await asyncio.wait(
                tasks, return_when=asyncio.FIRST_EXCEPTION)

        try:
            for task in done:
                # If one of the tasks exited with an exception
                # Calling .result() re-raises that exception
                task.result()
        finally:
            for task in pending:
                task.cancel()

    async def dispatch_incoming(self, queue: Queue,
                                request: Union[str, bytes]) -> None:
        """
        Dispatch incoming messages to the JSON-RPC method dispatcher.

        When the result is ready it is placed on the outgoing queue.
        """

        response = await dispatch(request, methods=self.methods)
        self.log.info(format_rpc_call(request, response, truncate_value=80))
        await queue.put(response)

    async def handle_incoming(self, websocket: WebSocketClientProtocol,
                              queue: Queue) -> None:
        """
        This coroutine checks the websocket for incoming JSON-RPC requests and
        passes them to `dispatch_incoming`.
        """

        while True:
            request = await websocket.recv()
            future = asyncio.ensure_future(
                    self.dispatch_incoming(queue, request))

            def callback(future):
                try:
                    future.result()
                except Exception as exc:
                    self.log.exception(
                        f'unhandled exception dispatching request '
                        f'{request!r}; this indicates an error in the RPC '
                        f'method implementation: {exc}')

            future.add_done_callback(callback)

    async def handle_outgoing(self, websocket: WebSocketClientProtocol,
                              queue: Queue) -> None:
        """
        This coroutine checks the outgoing response queue for results from
        dispatched RPC methods, and sends them on the websocket.
        """

        while True:
            response = await queue.get()
            if response.wanted:
                await websocket.send(str(response))

    def run(self) -> None:
        """
        Run the main event loop for the recommendation system.

        Starts by calling `initialize`, and once that is complete starts up
        the websocket connection and runs forever.
        """

        loop = asyncio.get_event_loop()
        try:
            while True:
                try:
                    loop.run_until_complete(self.initialize_if_needed())
                    loop.run_until_complete(self.request_loop())
                except (websockets.WebSocketException, ConnectionRefusedError):
                    self.log.warning('lost connection to the websocket server')
                    if self.RETRY_RATE is None:
                        break
                    else:
                        self.log.warning('trying to re-establish...')
                        time.sleep(self.RETRY_RATE)
        except KeyboardInterrupt:
            return
        finally:
            self.log.info('shutting down cleanly...')
            loop.run_until_complete(self.shutdown())
            loop.run_until_complete(shutdown(loop))
            loop.close()
            self.log.info('done')
