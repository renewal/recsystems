"""
Command-line script for commanding the Renewal Backend to send test calls to
your recsystem.
"""


import asyncio
import os.path as pth
import sys
from http import HTTPStatus
from pprint import pprint
from urllib.parse import urljoin

import aiohttp
import objclick as click  # type: ignore

from . import __version__
from .recsystem import RenewalRecsystem
from .utils import FileOrToken


class RecsystemTesterError(Exception):
    """
    Base class for error results from calls to the backend's RPC test API.
    """


class RecsystemTesterClientError(RecsystemTesterError):
    """
    Signals an error in the test client itself (e.g. incompatibility with
    the backend API).
    """

    def __str__(self):
        return f'error in test client: {self.args[0]}'


class RecsystemResponseError(RecsystemTesterError):
    """
    Raised when the recsystem returned an error in response to an RPC call.
    """

    def __init__(self, method, code, message):
        self.method = method
        self.code = code
        self.message = message

    def __str__(self):
        return (f'recsystem error on {self.method}() call: '
                f'({self.code}) {self.message}')


class RecsystemTester:
    def __init__(self, api_base_uri, token):
        # Make sure the base URI ends in a single slash
        self.api_base_uri = api_base_uri.rstrip('/') + '/'
        self.token = token

    async def rpc_test(self, method, **options):
        """
        Test the specified RPC method along with any additional JSON-encodable
        options.

        The valid options depend on the method being tested; currently this is
        just defined in the code for ``renewal_backend.web.api.v1.rpc_test``.
        """

        headers = {'Authorization': 'Bearer ' + self.token}
        url = urljoin(self.api_base_uri, f'rpc_test/{method}')
        known_statuses = (HTTPStatus.OK, HTTPStatus.BAD_REQUEST,
                          HTTPStatus.NOT_FOUND)

        async with aiohttp.ClientSession(headers=headers) as sess:
            async with sess.post(url, json=options) as resp:
                if resp.status not in known_statuses:
                    # known status codes for responses from the API
                    resp.raise_for_status()

                # Should return valid JSON
                result = await resp.json()

                if resp.status != HTTPStatus.OK:
                    raise RecsystemTesterClientError(result['error'])
                elif 'error' in result:
                    raise RecsystemResponseError(result['method'],
                                                 **result['error'])

                return result['result']

    def print_rpc_result(self, method, **options):
        """
        Wrapper for `rpc_test` which prints the result (if any) to stdout, or
        an error to stderr.
        """

        try:
            result = asyncio.run(self.rpc_test(method, **options))
        except RecsystemTesterError as exc:
            print(exc, file=sys.stderr)
            sys.exit(1)
        except Exception as exc:
            print(f'unexpected error: {exc}')
            sys.exit(1)

        if result is not None:
            pprint(result, compact=True)

    @click.classgroup()
    @click.version_option(__version__, message='%(version)s')
    @click.option('-a', '--api-base-uri',
                  default=RenewalRecsystem.RENEWAL_API_BASE_URI,
                  help='URI for the Renewal HTTP API')
    @click.option('-t', '--token', required=True, type=FileOrToken(),
                  help='authentication token for the recsystem; if a valid '
                       'filename is given the token is read from a file '
                       'instead')
    def main(cls, api_base_uri, token):
        """
        Command-line script for commanding the Renewal Backend to send test
        calls to your recsystem.

        You can use this to make on-demand tests of the remote calls the
        backend makes to your recsystem, such as recommend().

        It will also log the response (if any) that your recsystem sent to the
        backend.

        Notification methods that do not return a response (such as
        new_article() or article_interaction() thus do not return anything,
        whereas test recommend() calls will print the returned recommendations
        from your recsystem.

        Each RPC call you can test is a sub-command of this command (e.g.
        recommend) and some take optional arguments to control with what
        arguments the calls are made.
        """

        return cls(api_base_uri, token.read().strip())

    @main.command()
    def ping(self):
        self.print_rpc_result('ping')

    @main.command()
    def stats(self):
        self.print_rpc_result('stats')

    @main.command()
    @click.option('--user-id', default='fake-user',
                  help='user ID of user to request recommendations for')
    def recommend(self, user_id='fake-user'):
        self.print_rpc_result('recommend', user_id=user_id)

    @main.command()
    @click.option('--user-id', default='fake-user',
                  help='user ID of user sending the article interaction')
    @click.option('--article-id', type=int,
                  help='article ID of the article the user interacted with '
                       '(if omitted an arbitrary article is used)')
    @click.option('--clicked', is_flag=True,
                  help='generate an interaction in which the user clicked an '
                       'article (the default if no other flags are used)')
    @click.option('--bookmarked', is_flag=True,
                  help='generate an interaction in which the user bookmarked '
                       'an article')
    # NOTE: Kind of annoying that click.Choice does not accept ints
    @click.option('--rating', type=click.Choice(['-1', '0', '1']),
                  help='generate an interaction in which the user rated an '
                       'article (-1, 0, 1) where 0 means they cleared a '
                       'previous rating')
    @click.option('--prev-rating', type=click.Choice(['-1', '0', '1']),
                  default='0',
                  help="when using --rating, the user's previous rating on "
                       "the article (default: 0 / unrated)")
    def article_interaction(self, user_id='fake-user', article_id=None,
                            clicked=False, bookmarked=False, rating=None,
                            prev_rating=0):
        options = {'user_id': user_id}

        if article_id is not None:
            options['article_id'] = article_id

        intr = {}
        if clicked:
            intr['clicked'] = True

        if bookmarked:
            intr['bookmarked'] = True

        if rating is not None:
            intr['rating'] = int(rating)
            intr['prev_rating'] = int(prev_rating)

        if intr:
            options['interaction'] = intr

        self.print_rpc_result('article_interaction', **options)

    @main.command()
    @click.option('--article-id', type=int,
                  help='send a specific article as a "new" article; otherwise '
                       'an arbitrary article is sent')
    def new_article(self, article_id=None):
        self.print_rpc_result('new_article', article_id=article_id)

    @main.command()
    @click.option('--user-id', default='fake-user',
                  help='user ID of user assigned to the recsystem')
    def assigned_user(self, user_id='fake-user'):
        self.print_rpc_result('assigned_user', user_id=user_id)

    @main.command()
    @click.option('--user-id', default='fake-user',
                  help='user ID of user unassigned from the recsystem')
    def unassigned_user(self, user_id='fake-user'):
        self.print_rpc_result('unassigned_user', user_id=user_id)


if __name__ == '__main__':
    RecsystemTester.main(
        auto_envvar_prefix=RenewalRecsystem.ENVVAR_PREFIX,
        prog_name=f'{pth.basename(sys.executable)} -m {__package__}.test'
    )
