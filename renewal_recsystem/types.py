"""Some basic data types used in recystems."""


from collections import defaultdict
from dataclasses import dataclass, field
from typing import DefaultDict


ArticleId = int
UserId = str


@dataclass
class User:
    uid: UserId
    """User's unique UID."""

    interactions: DefaultDict[int, dict] = \
        field(default_factory=lambda: defaultdict(dict))
    """
    Maps article IDs to the user's interactions with those articles.

    For example::

        {
            12345: {
                "recommended": True,
                "rating": 1,
                "clicked": True
                "bookmarked": False
            }
        }
    """
