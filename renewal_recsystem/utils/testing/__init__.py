"""Utilities used only for the tests."""


import asyncio
import contextlib
import itertools
import json
import logging
import os
import random
import socket
import string
import sys
import tempfile
import time
import threading
from datetime import datetime

import websockets

from ..utils import shutdown

try:
    from jsonrpcclient.clients.websockets_client import WebSocketsClient
    from jsonrpcclient.response import Response
    import lorem  # type: ignore
except ImportError:
    try:
        if __sphinx_build__:  # type: ignore[name-defined]
            # Set a dummy for WebSocketsClient
            class WebSocketsClient:  # type: ignore[no-redef]
                __module__ = 'jsonrpcclient.clients.websockets_client'
    except NameError:
        raise ImportError(
            __name__,
            'renewal_recsystem[tests] must be installed to use this module')


class TempDirTests:
    """
    Base class with setup/teardown_class methods for running all tests for
    a class in a temp dir.

    This is used instead of a fixture since pytest fixtures for class
    setup/teardown are kind of confusing.
    """

    @classmethod
    def setup_class(cls):
        cls.prev_cwd = os.getcwd()
        tmpdir = tempfile.mkdtemp()
        os.chdir(tmpdir)

    @classmethod
    def teardown_class(cls):
        if hasattr(cls, 'prev_cwd'):
            os.chdir(cls.prev_cwd)
            delattr(cls, 'prev_cwd')


@contextlib.contextmanager
def get_free_port_safe():
    """
    Returns a random free TCP port and holds it for the application until
    the context manager exits.

    This opens a socket with the ``SO_REUSEADDR`` set, so this requires that
    the code actually using this port also sets ``SO_REUSEADDR`` for the
    corresponding socket.

    Examples
    --------

    When using `get_free_port_safe` it should be guaranteed that you can
    bind a new socket to the returned port, so long as ``SO_REUSEADDR`` is
    set on the socket:

    >>> from renewal_recsystem.utils.testing import get_free_port_safe
    >>> import socket
    >>> with get_free_port_safe() as port:
    ...     sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ...     sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    ...     sock.bind(('localhost', port))
    ...

    Otherwise it will result in an exception:

    >>> with get_free_port_safe() as port:
    ...     sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ...     sock.bind(('localhost', port))
    ...
    Traceback (most recent call last):
    ...
    OSError: [Errno ...] Address already in use
    """

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(('0.0.0.0', 0))
        _, port = sock.getsockname()
        yield port


def _websocket_test_protocol(protocol_setup, protocol_body,
                             ready_callback=None):
    """
    Internal implementation of `websocket_test_server` and
    `websocket_test_client`.  The difference is just whether it runs as a
    server or as a client.
    """

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    async def protocol():
        async with protocol_setup() as proto:
            if ready_callback is not None:
                ready_callback()

            await protocol_body(proto)

    try:
        loop.run_until_complete(protocol())
    finally:
        # Shut down the event loop
        loop.run_until_complete(shutdown(loop))
        loop.close()


def websocket_test_server(handler, ready_callback=None, **kwargs):
    """
    A simple websocket server for testing in its own event loop.

    Intended to be run in a thread, the ``ready_callback`` is expected to be a
    callable which when called signals to the callee that the server is ready
    to accept connections (e.g. pass a bound `threading.Event.set` method).
    By design, the ``handler`` is only run once, and then the server shuts
    down.

    ``handler`` is a function accepting two arguments which handles the
    websocket connection, same as the ``ws_handler`` argument to
    `websockets.server.serve`.

    Additional keyword arguments are passed either to `websockets.server.serve`
    or `websockets.server.unix_serve` if a ``path`` argument is provided
    (giving the path to a UNIX socket).
    """

    def proto_setup():
        handled_one = asyncio.Future()

        async def wrapped_handler(ws, path):
            try:
                await handler(ws, path)
            finally:
                handled_one.set_result(True)

        path = kwargs.pop('path', None)
        if path is None:
            server = websockets.serve(wrapped_handler, **kwargs)
        else:
            server = websockets.unix_serve(wrapped_handler, path, **kwargs)

        # This is hack to return a "server" async context manager that just
        # runs until the handler() has been called once, triggering the
        # handled_one future
        @contextlib.asynccontextmanager
        async def handle_one_request():
            async with server:
                yield handled_one

        return handle_one_request()

    async def proto_body(handle_one):
        await handle_one

    return _websocket_test_protocol(proto_setup, proto_body, ready_callback)


def websocket_test_client(handler, ready_callback=None, **kwargs):
    """
    Like `websocket_test_server` but starts a websocket client.

    The ``handler`` function is passed simply the websocket object.
    """

    def proto_setup():
        path = kwargs.pop('path', None)
        if path is None:
            return websockets.connect(**kwargs)
        else:
            return websockets.unix_connect(path, **kwargs)

    async def proto_body(client):
        await handler(client)

    return _websocket_test_protocol(proto_setup, proto_body, ready_callback)


def _websocket_test_thread(target, handler, timeout=5, **kwargs):
    """
    Runs `websocket_test_server` in a thread and waits until the server is
    ready to receive RPC calls.

    Returns the thread itself.
    """

    ready_event = threading.Event()
    thread = threading.Thread(target=target, args=(handler, ready_event.set),
                              kwargs=kwargs)
    thread.daemon = True
    thread.start()
    ready_event.wait(timeout=timeout)
    return thread


def websocket_test_server_thread(handler, timeout=5, **kwargs):
    """
    Runs `websocket_test_server` in a thread and waits until the server is
    ready to receive RPC calls.

    Returns the thread itself.
    """

    return _websocket_test_thread(websocket_test_server, handler,
                                  timeout=timeout, **kwargs)


def websocket_test_client_thread(handler, timeout=5, **kwargs):
    """
    Runs `websocket_test_client` in a thread and waits until the client is
    ready to send RPC calls.

    Returns the thread itself.
    """

    return _websocket_test_thread(websocket_test_client, handler,
                                  timeout=timeout, **kwargs)


class WebSocketsMultiClient(WebSocketsClient):
    """
    Implements a ``jsonrpcclient`` client with support for handling multiple
    ongoing requests concurrently.

    See https://github.com/bcb/jsonrpcclient/issues/154 for a more detailed
    explanation.

    See the doctest in `~renewal_recsystem.server.JSONRPCServerWebsocketClient`
    for example usage.
    """

    def __init__(self, socket, *args, fallback_handler=None, **kwargs):
        super().__init__(socket, *args, **kwargs)
        self.pending_responses = {}
        self.fallback_handler = fallback_handler
        self.receiving = None

    async def __aenter__(self):
        self.receiving = asyncio.ensure_future(self.receive_responses())
        return self

    async def __aexit__(self, *args):
        self.receiving.cancel()
        # Wait for the receive responses task to cancel
        try:
            await self.receiving
        except asyncio.CancelledError:
            pass

    async def receive_responses(self):
        while True:
            try:
                response_text = await self.socket.recv()
            except websockets.ConnectionClosedOK:
                # The websocket was closed on the server side before shutting
                # down (i.e. with __aexit__); just break out of the loop and
                # exit
                return
            try:
                # Not a proper JSON-RPC response so we just ignore it since
                # during this mode all messages received from the socket
                # should be RPC responses
                response = json.loads(response_text)
                # Identify responses to batch requests by the first ID in
                # the batch
                if response and isinstance(response, list):
                    response_id = response[0]['id']
                else:
                    response_id = response['id']
            except (json.JSONDecodeError, KeyError):
                if self.fallback_handler is not None:
                    await self.fallback_handler(response_text)
                continue

            await self.pending_responses[response_id].put(response_text)

    async def send(self, request, **kwargs):
        if self.receiving is None:
            raise RuntimeError(
                f'{self} must be used in an async with statement context '
                f'before it can be used to make RPC calls')
        kwargs['request_id'] = request.get('id', None)
        return await super().send(request, **kwargs)

    async def send_message(self, request, response_expected, request_id=None,
                           **kwargs):
        if response_expected:
            queue = self.pending_responses[request_id] = asyncio.Queue()

        await self.socket.send(request)

        if response_expected:
            # As a sanity measure, wait for both the receive_responses task and
            # the queue.  If the receive_responses task returns first that
            # typically means an error occurred (e.g. the websocket was closed)
            # If the completed task was receive_responses, when we call
            # result() it will raise any exception that occurred.
            get_queue = asyncio.create_task(queue.get())
            pending = set([get_queue, self.receiving])

            # Set a default empty response, since the server could send a
            # bogus response then close before a good response is received
            response = ''
            try:
                done, pending = await asyncio.wait(
                        pending,
                        return_when=asyncio.FIRST_COMPLETED)

                for task in done:
                    # Raises an exception if task is self.receiving since
                    # it otherwise should never return
                    result = task.result()
                    if task is not self.receiving:
                        # Response from the queue
                        response = result
            except BaseException:
                for task in pending:
                    task.cancel()
                await asyncio.gather(*pending, return_exceptions=True)

            del self.pending_responses[request_id]
            return Response(response)
        else:
            return Response('')

    def basic_logging(self):
        """
        The base class sets the default log handlers to print to stderr, but
        they need to print to stdout in order for log messages to show up
        in doctests.
        """

        super().basic_logging()

        from jsonrpcclient.client import request_log, response_log
        for log in (request_log, response_log):
            for handler in log.handlers:
                if isinstance(handler, logging.StreamHandler):
                    handler.setStream(sys.stdout)


def generate_articles(start_id=1):
    """
    Iterator which returns random articles with increasing article_ids.

    Examples
    --------

    >>> from renewal_recsystem.utils.testing import generate_articles
    >>> from pprint import pprint
    >>> import random
    >>> random.seed(0)
    >>> articles = generate_articles()
    >>> pprint(next(articles))
    {'article_id': 1,
     'authors': ['Occaecat Mollit'],
     'date': '...-...-...T...:...:...',
     'image_url': 'https://example.com/1/top_image.png',
     'keywords': ['proident', 'id', 'duis'],
     'lang': 'en',
     'metrics': {'bookmarks': 0,
                 'clicks': 0,
                 'dislikes': 0,
                 'likes': 0,
                 'recommends': 0},
     'site': {'icon_url': 'https://localhost/v1/images/icons/0123456789abcdef',
              'name': 'Example',
              'url': 'example.com'},
     'summary': 'Ad aliquip ullamco pariatur...'
                ...
                'fugiat commodo nostrud non incididunt nisi.',
     'text': 'Eiusmod fugiat cupidatat elit esse ipsum do velit...'
             ...
             'aliqua occaecat sed laborum.',
     'title': 'Ad Nisi Ut Pariatur Voluptate',
     'url': 'https://example.com/1'}
    >>> pprint(next(articles))
    {'article_id': 2,
     ...
     'url': 'https://example.com/2'}
    """

    # NOTE: This should be kept in sync with the backend API implementation
    template = {
        'article_id': 0,
        'authors': [],
        'date': datetime.utcnow().isoformat(timespec='seconds'),
        'image_url': 'https://example.com/{article_id}/top_image.png',
        'keywords': [],
        'lang': 'en',
        'metrics': {
            'bookmarks': 0,
            'clicks': 0,
            'dislikes': 0,
            'likes': 0,
            'recommends': 0
        },
        'site': {
            'icon_url':
                'https://localhost/v1/images/icons/0123456789abcdef',
            'name': 'Example',
            'url': 'example.com'
         },
        'summary': '',
        'text': '',
        'title': '',
        'url': 'https://example.com/{article_id}'
    }

    counter = itertools.count(start_id)

    while True:
        article = template.copy()
        article_id = next(counter)
        article.update({
            'article_id': article_id,
            'authors': [lorem.get_word(2, func='capitalize')],
            'image_url': article['image_url'].format(article_id=article_id),
            'keywords': lorem.get_word(3).split(),
            'summary': lorem.get_paragraph(),
            'text': lorem.get_paragraph(),
            'title': lorem.get_word(5, func='capitalize'),
            'url': article['url'].format(article_id=article_id)
        })
        yield article


def random_user_id(length=28, chars=(string.ascii_letters + string.digits)):
    """
    Generate a random user ID in the format used by Firebase.

    Example
    -------

    >>> from renewal_recsystem.utils.testing import random_user_id
    >>> import random
    >>> random.seed(0)
    >>> random_user_id()
    '2yW4AcqGFzYtEwLn9isSgN3IjZPe'
    """

    return ''.join(random.sample(chars, length))


def retry(count=0, interval=None, exc_type=BaseException):
    """
    Decorator which retries the wrapped function up to ``count`` times if
    an exception occurs.

    The wrapped function should not require any arguments and is executed
    right away--this is a workaround to the fact that it is not easy to write
    a context manager to re-execute code (it is possible to do this but not
    without massive hacks; see
    https://gist.github.com/embray/f95d9cbe677542ce19a1).

    If given, will wait ``interval`` seconds before each retry.

    If ``exc_type`` is given it will only retry if the exception is a subclass
    of the given exception type.

    Examples
    --------

    >>> from renewal_recsystem.utils.testing import retry
    >>> tries = 0
    >>> def test_func():
    ...     global tries
    ...     tries += 1
    ...     assert tries > 3
    ...
    >>> @retry(count=3, interval=0.1)
    ... def retry_test_func():
    ...     test_func()
    ...
    >>> tries
    4
    >>> tries = 0
    >>> @retry(count=2, interval=0.1)
    ... def retry_test_func():
    ...     test_func()
    ...
    Traceback (most recent call last):
    ...
    AssertionError
    >>> tries
    3

    If the error doesn't match ``exc_type`` then the function won't be retried:

    >>> tries = 0
    >>> @retry(count=10, exc_type=RuntimeError)
    ... def retry_test_func():
    ...     test_func()
    ...
    Traceback (most recent call last):
    ...
    AssertionError
    """

    def decorator(func):
        nonlocal count

        while True:
            try:
                return func()
            except BaseException as exc:
                if not count or not isinstance(exc, exc_type):
                    raise

                if interval is not None:
                    time.sleep(interval)

                count -= 1

        # Immediately call the wrapped function--retry starts running as
        # as soon as a function is decorated
        return func

    return decorator
