"""
Implements a base class for a suite of tests of the basic recsystem, which can
also be used to test alternative subclasses of
`~renewal_recsystem.basic.BasicRecsystem`.
"""


import abc
import asyncio
import itertools
from typing import Type

try:
    # Python 3.8+ only
    from unittest.mock import AsyncMock  # type: ignore
except ImportError:
    try:
        from asyncmock import AsyncMock  # type: ignore
    except ImportError:
        raise ImportError(
            __name__,
            'renewal_recsystem[tests] must be installed to run these tests')

import pytest

from ...articles import ArticleCollection
from ...basic import BasicRecsystem
from ...types import User
from . import generate_articles, random_user_id, TempDirTests


# It is not possible to use the monkeypatch fixture in a class-scoped fixture;
# see https://github.com/pytest-dev/pytest/issues/363 so we have to implement
# our own class-scoped monkeypatch
@pytest.fixture(scope='class')
def monkeyclass(request):
    from _pytest.monkeypatch import MonkeyPatch
    mpatch = MonkeyPatch()
    yield mpatch
    mpatch.undo()


@pytest.fixture(scope='class')
def recsys(request, monkeyclass):
    """
    Test fixture to construct a `.BasicRecsystem` instance shared by all
    tests in the class.

    This is meant to be used with `TestBaselineRecsystem` which can be
    subclassed.
    """

    recsystem = request.cls.RECSYSTEM_CLS(
            request.cls.RECSYSTEM_MODULE,
            api_base_uri='http://localhost/v1',
            token='fake.token'
    )

    # mock the initial backend API requests
    def get(self, url, *args, params={}, **kwargs):
        if url.endswith('/articles'):
            # For the purposes of this test max_id and since_id are currently
            # ignored since they are not used by the code under test
            limit = params.pop('limit', request.cls.DEFAULT_ARTICLES_LIMIT)
            json = list(itertools.islice(generate_articles(), limit))
        elif url.endswith('/user_assignments'):
            json = [{'user_id': random_user_id(), 'interactions': []}
                    for _ in range(request.cls.N_ASSIGNED_USERS)]
        else:
            json = None

        response = AsyncMock()
        response.__aenter__ = AsyncMock(return_value=response)
        response.json = AsyncMock(return_value=json)
        return response

    monkeyclass.setattr('aiohttp.ClientSession.get', get)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(recsystem.initialize_if_needed())
    return recsystem


class BasicRecsystemTest(TempDirTests, metaclass=abc.ABCMeta):
    """
    Unit tests of the `.BasicRecsystem`'s initialization and RPC methods.

    For the purpose of these test we don't actually run the main client loop
    (`BasicRecsystem.run`) as its functionality is tested by tests of the
    lower-level server code.

    This just tests the results of calling the individual RPC methods directly
    as though they were called by an RPC client.
    """

    RECSYSTEM_CLS: Type[BasicRecsystem] = BasicRecsystem
    """
    This should be assigned to `~renewal_recsystem.basic.BasicRecsystem` or a
    subclass thereof.
    """

    @property
    @abc.abstractmethod
    def RECSYSTEM_MODULE(self) -> str:
        """The name of the recsystem hooks module to load for the test."""

    DEFAULT_ARTICLES_LIMIT = 30
    N_ASSIGNED_USERS = 100

    def run(self, coro):
        """
        Run an async function call on the event loop and return the results.
        """

        return asyncio.get_event_loop().run_until_complete(coro)

    def test_initialize(self, recsys):
        assert isinstance(recsys.articles, ArticleCollection)
        assert len(recsys.articles) == recsys.INITIAL_ARTICLES

        assert isinstance(recsys.users, dict)
        assert len(recsys.users) == self.N_ASSIGNED_USERS

    def test_ping(self, recsys):
        """Test the ping RPC endpoint."""

        assert self.run(recsys.ping()) == 'pong'

    def test_new_article(self, recsys):
        """Test the new_article RPC endpoint."""

        # generate a few new articles with an IDs a little bit higher than the
        # heighest article ID currently in the collection:
        last_id = recsys.articles.article_ids[-1]
        last_len = len(recsys.articles)

        article_generator = generate_articles(last_id + 1)
        new_articles = list(itertools.islice(article_generator, 5))

        # push the new article with the highest ID; this will will create a
        # "gap" in the article_ids of stored articles
        self.run(recsys.new_article(new_articles[-1]))
        assert len(recsys.articles) == last_len + 1

        # take a slice
        slc = list(recsys.articles[last_id:])
        assert len(slc) == 2
        assert slc[0] == recsys.articles[new_articles[-1]['article_id']]
        assert slc[1] == recsys.articles[last_id]

        # insert an article with a lower article_id and ensure it gets sorted
        # into the "gap" (e.g. testing new articles delivered out of order from
        # their article_id
        self.run(recsys.new_article(new_articles[2]))
        assert len(recsys.articles) == last_len + 2

        # take a slice
        slc = list(recsys.articles[last_id:])
        assert len(slc) == 3
        assert slc[0] == recsys.articles[new_articles[-1]['article_id']]
        assert slc[1] == recsys.articles[new_articles[2]['article_id']]
        assert slc[2] == recsys.articles[last_id]

    def test_article_interaction(self, recsys):
        """Test the article_interaction RPC endpoint."""

        # Test a basic sequence of interactions on a single article
        article_id = recsys.articles.article_ids[-1]

        # Template article_interaction object
        user_id = random_user_id()
        recsys.users[user_id] = User(user_id)
        template = {'user_id': user_id, 'article_id': article_id}

        # Shortcut method to generate an interaction object and pass it to
        # the article_interaction RPC
        def interaction(**kwargs):
            interaction = template.copy()
            interaction.update(kwargs)
            self.run(recsys.article_interaction(interaction))

        # Each article_interaction will update the tally of article_metrics
        # in-place, so we just need to make sure it is updated properly for
        # each interaction type
        metrics = recsys.articles[article_id]['metrics']
        user_data = recsys.users[user_id].interactions[article_id]

        # The default we should be starting from
        assert metrics == {'likes': 0, 'dislikes': 0, 'clicks': 0,
                           'bookmarks': 0, 'recommends': 0}

        interaction(recommended=True)
        assert metrics['recommends'] == 1
        assert user_data['recommended'] is True

        interaction(rating=1, prev_rating=0)
        assert metrics['likes'] == 1 and metrics['dislikes'] == 0
        assert user_data['rating'] == 1

        interaction(rating=1, prev_rating=0)
        assert metrics['likes'] == 2 and metrics['dislikes'] == 0
        assert user_data['rating'] == 1

        interaction(rating=-1, prev_rating=1)
        assert metrics['likes'] == 1 and metrics['dislikes'] == 1
        assert user_data['rating'] == -1

        interaction(rating=0, prev_rating=-1)
        assert metrics['likes'] == 1 and metrics['dislikes'] == 0
        assert user_data['rating'] == 0

        interaction(rating=-1, prev_rating=0)
        assert metrics['likes'] == 1 and metrics['dislikes'] == 1
        assert user_data['rating'] == -1

        interaction(bookmarked=True)
        assert metrics['bookmarks'] == 1
        assert user_data['bookmarked'] is True

        interaction(bookmarked=True)
        assert metrics['bookmarks'] == 2
        assert user_data['bookmarked'] is True

        interaction(bookmarked=False)
        assert metrics['bookmarks'] == 1
        assert user_data['bookmarked'] is False

        interaction(clicked=True)
        assert metrics['clicks'] == 1
        assert user_data['clicked'] is True

    def test_recommend(self, recsys):
        """Test the recommend RPC endpoint."""

        # TODO: This test is bare-bones right now.  It needs to be fleshed out
        # to ensure
        # 1) The returned recommendations are according to the pattern expected
        #    by the active recommendation algorithm.
        # 2) A specific user is not returned the same recommendations more than
        #    once.
        user_id = random_user_id()
        recsys.users[user_id] = User(user_id)
        recs = self.run(recsys.recommend(user_id=user_id))
        assert len(recs) <= recsys.RECOMMEND_DEFAULT_MAX_ARTICLES
        assert len(recs) >= recsys.RECOMMEND_DEFAULT_MIN_ARTICLES
        assert len(recs) == len(set(recs))
        for rec in recs:
            assert rec in recsys.articles

    # The following two tests are pretty trivial
    def test_assigned_user(self, recsys):
        """Test the assigned_user RPC endpoint."""

        user_id = random_user_id()
        self.run(recsys.assigned_user(user_id))
        assert user_id in recsys.users

    def test_unassigned_user(self, recsys):
        """Test the unassigned_user RPC endpoint."""

        user_id = random_user_id()
        self.run(recsys.assigned_user(user_id))
        assert user_id in recsys.users
        self.run(recsys.unassigned_user(user_id))
        assert user_id not in recsys.users

        # Try to unassign the user again--this results in a logged
        # warning but should not otherwise be a problem:
        self.run(recsys.unassigned_user(user_id))
        assert user_id not in recsys.users

    def test_shutdown(self, recsys):
        """Run the recsystem's shutdown method.  This test must be last."""

        self.run(recsys.shutdown())
