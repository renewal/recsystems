import asyncio
import io
import json
from collections import OrderedDict

import jwt
import objclick as click  # type: ignore
from jsonrpcserver.response import DictResponse


__all__ = ['shutdown', 'format_rpc_call', 'FileOrToken', 'inherit_docstring',
           'dict_merge', 'LRUDict']


async def shutdown(loop):
    """
    Cleanly shut down `asyncio` loop, ensuring all pending tasks are
    canceled.
    """

    while True:
        # The while loop handles some race conditions (e.g. a bug in
        # hypercorn that is fixed in later versions:
        # https://gitlab.com/pgjones/hypercorn/-/commit/dbaf6a4bc372522db9615375698d1613e43e609d
        # that could result in some new tasks being spawned during shutdown
        # that aren't properly awaited when canceled
        tasks = [t for t in asyncio.all_tasks(loop)
                 if t is not asyncio.current_task(loop)]

        if not tasks:
            break

        for t in tasks:
            t.cancel()

        await asyncio.gather(*tasks, return_exceptions=True)

    await loop.shutdown_asyncgens()
    loop.stop()


def format_rpc_call(request, response=None, truncate_value=None):
    """
    For debugging purposes, print parsed JSON-RPC requests/responses.

    If given, ``truncate_long=<int>`` will truncate output of long string
    values to the specified number of characters.  This can reduce some noise
    in logs.

    Examples
    --------

    >>> from renewal_recsystem.utils import format_rpc_call
    >>> req = {'jsonrpc': '2.0', 'method': 'add',
    ...        'params': [2, 2], 'id': 1}
    >>> res = {'jsonrpc': '2.0', 'result': 5, 'id': 1}
    >>> print(format_rpc_call(req, res))
    add(2, 2) -> 5

    >>> req = {'jsonrpc': '2.0', 'method': 'hello',
    ...        'params': {'name': 'Fred'}, 'id': 2}
    >>> res = {'jsonrpc': '2.0', 'result': 'Hello Fred!', 'id': 2}
    >>> print(format_rpc_call(req, res))
    hello(name='Fred') -> 'Hello Fred!'

    >>> req = {'jsonrpc': '2.0', 'method': 'hello',
    ...        'params': {'name': [1, 2, 3]}, 'id': 3}
    >>> res = {'jsonrpc': '2.0', 'error': 'TypeError: expected str not list',
    ...        'id': 3}
    >>> print(format_rpc_call(req, res))
    hello(name=[1, 2, 3]) !! TypeError: expected str not list
    """

    if isinstance(request, str):
        request = json.loads(request)

    if isinstance(response, DictResponse):
        response = response.deserialized()
    elif not isinstance(response, dict):
        response = None

    method = request['method']
    params = request.get('params', {})

    def format_value(val):
        if truncate_value:
            if isinstance(val, str):
                if len(val) > truncate_value:
                    val = val[:truncate_value] + '... <truncated>'
            elif isinstance(val, (list, tuple)):
                val = [format_value(v) for v in val]
            elif isinstance(val, dict):
                val = {k: format_value(v) for k, v in val.items()}

        return val

    if isinstance(params, list):
        params = ', '.join(str(format_value(v)) for v in params)
    else:
        params = ', '.join(f'{k}={format_value(v)!r}'
                           for k, v in params.items())
    call = f'{method}({params})'

    if response is None:
        return call

    if 'error' in response:
        return f'{call} !! {response["error"]}'
    else:
        return f'{call} -> {response["result"]!r}'


class FileOrToken(click.File):
    """
    Extends `click.File` to also accept a JWT token.

    If the input value resembles a properly formatted JWT token its value will
    be taken as-is wrapped in an `io.StringIO`.  Otherwise the input is assumed
    to be a filename and the file is returned as an open file object.

    Examples
    --------

    >>> import click, jwt
    >>> from renewal_recsystem.utils import FileOrToken

    Generate a fake JWT:

    >>> my_token = jwt.encode({}, 'secret-key').decode('ascii')

    Make a command that takes a JWT/filename as an argument:

    >>> @click.command()
    ... @click.argument('token', type=FileOrToken())
    ... def main(token):
    ...     with token as fobj:
    ...         token = fobj.read()
    ...     print(token == my_token)
    ...
    >>> main([my_token], standalone_mode=False)
    True

    Write the token to a file and try passing it as a filename instead:

    >>> tmp_path = getfixture('tmp_path')  # pytest specific
    >>> filename = tmp_path / 'my_token.jwt'
    >>> with filename.open('w') as fobj:
    ...     _ = fobj.write(my_token)
    >>> main([str(filename)], standalone_mode=False)
    True

    Something that is neither a token nor a real file:

    >>> main(['not-a-token-at-all'], standalone_mode=False)
    Traceback (most recent call last):
    ...
    click.exceptions.BadParameter: Could not open file: not-a-token-at-all:
    No such file or directory
    """

    def convert(self, value, param, ctx):
        try:
            jwt.decode(value, verify=False)
        except jwt.DecodeError:
            return super().convert(value, param, ctx)

        return io.StringIO(value)


def inherit_docstring(orig_func):
    """
    A simple decorator for setting the docstring of one function from another.

    Does not use `functools.wraps` to avoid setting the ``__wrapped__``
    attribute since this is not meant for use with wrapped functions; just
    copying the docstring.

    Examples
    --------

    >>> from renewal_recsystem.utils import inherit_docstring
    >>> def orig_func():
    ...     \"\"\"Orig docstring.\"\"\"
    ...
    >>> @inherit_docstring(orig_func)
    ... def new_func(): pass
    ...
    >>> new_func.__doc__
    'Orig docstring.'
    """

    def decorator(func):
        func.__doc__ = orig_func.__doc__
        return func

    return decorator


def dict_merge(d, *f):
    """
    Return a copy of `dict` ``d``, with updates applied to it from zero or more
    dicts in ``f``.

    If ``d2`` is a nested `dict` then updates are applied partially, so that
    rather than completely replacing the value of the same key in ``d1``, it
    just applies a `dict.update` to value of that key in ``d1``.  If there is
    a conflict (e.g. the same key in each `dict` does not correspond to the
    same type) then that value is replaced entirely.

    Examples
    --------

    >>> from renewal_recsystem.utils import dict_merge
    >>> d1 = {}
    >>> d2 = dict_merge(d1); d2
    {}
    >>> d1 is d2
    False
    >>> dict_merge({}, {'a': 1})
    {'a': 1}
    >>> dict_merge({'a': 1, 'b': 2}, {'a': 2, 'c': 3})
    {'a': 2, 'b': 2, 'c': 3}
    >>> dict_merge({'a': 1, 'b': 2}, {'a': 2}, {'a': 3, 'c': 4})
    {'a': 3, 'b': 2, 'c': 4}
    >>> dict_merge({'a': {'b': {'c': 1, 'd': 2}}}, {'a': {'b': {'c': 2}}})
    {'a': {'b': {'c': 2, 'd': 2}}}
    >>> dict_merge({'a': {'b': 1}}, {'a': 2})
    {'a': 2}
    """

    d = d.copy()
    for d2 in f:
        for k, v in d2.items():
            u = d.get(k)
            if isinstance(u, dict) and isinstance(v, dict):
                d[k] = dict_merge(u, v)
            else:
                d[k] = v

    return d


class LRUDict(OrderedDict):
    """
    `dict` which keeps up to ``maxsize`` of the most recently accessed entries.

    It is based on `collections.OrderedDict` due to its optimization for
    reordering.

    Examples
    --------

    >>> from renewal_recsystem.utils import LRUDict
    >>> d = LRUDict({'a': 1, 'b': 2}, maxsize=2)
    >>> d['b']
    2
    >>> d['c'] = 3
    >>> d
    LRUDict([('b', 2), ('c', 3)])
    >>> d['b']
    2
    >>> d['d'] = 4
    >>> d
    LRUDict([('b', 2), ('d', 4)])

    An additional feature is the ``del_callback`` argument.  When an element is
    removed from the `LRUDict` its key and value are passed to a provided
    callback function:

    >>> def del_callback(k, v):
    ...     print(f'removed element {{{k!r}: {v!r}}} from the LRUDict')
    >>> d = LRUDict({'a': 1, 'b': 2}, maxsize=2, del_callback=del_callback)
    >>> d['c'] = 3
    removed element {'a': 1} from the LRUDict
    >>> d
    LRUDict([('b', 2), ('c', 3)])
    """

    def __init__(self, *args, maxsize=128, del_callback=None, **kwargs):
        assert maxsize > 0
        self.maxsize = maxsize
        self.del_callback = del_callback
        super().__init__(*args, **kwargs)

    def __getitem__(self, key):
        value = super().__getitem__(key)
        self.move_to_end(key)
        return value

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        self.move_to_end(key)

        # Delete old keys until we are down to maxsize
        while self and len(self) > self.maxsize:
            oldest_key, oldest_value = next(iter(self.items()))
            del self[oldest_key]
            if callable(self.del_callback):
                self.del_callback(oldest_key, oldest_value)
