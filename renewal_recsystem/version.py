"""
Determine the package version.

`setuptools_scm` is used programmatically only if we are in the source
repository as determined by the presence of a ``.root`` file containing
a specific hard-coded string.

Otherwise the statically generated version from ``_version.py`` is used.
"""


import pathlib


root_magic = '0bed75b622cc0ba48e0bf4cb56b5108098ed362b'  # randomly generated
root_file = pathlib.Path(__file__).parent.parent / '.root'
use_setuptools_scm = False

if root_file.is_file():
    with root_file.open() as f:
        for line in f:
            if line.startswith('#'):
                continue
            elif line.rstrip() == root_magic:
                try:
                    import setuptools_scm  # type: ignore
                    use_setuptools_scm = True
                except ImportError:
                    pass
                break


if use_setuptools_scm:
    version = setuptools_scm.get_version(root='..', relative_to=__file__)
else:
    try:
        from ._version import version
    except ImportError:
        import warnings
        warnings.warn(
            f'could not determine package version of {__package__}; this '
            f'suggests a broken installation')
        version = '0.0.0'
