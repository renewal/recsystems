#!/usr/bin/env python
"""
A minimal ``setup.py`` is still required when using setuptools.

See ``setup.cfg`` for package configuration.
"""

from setuptools import setup
setup(use_scm_version={
    'local_scheme': 'node-and-timestamp',
    'write_to': 'renewal_recsystem/_version.py'
})
