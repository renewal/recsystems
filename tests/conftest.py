"""
Import various fixtures to ensure that they're registered for use in
tests.
"""


from renewal_recsystem.utils.testing.basic import (  # noqa: F401
        recsys, monkeyclass)
