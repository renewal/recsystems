"""Tests of `BaselineRecsystem`."""


import pickle
import os
import sys

from renewal_recsystem.basic import BasicRecsystem as _BasicRecsystem
from renewal_recsystem.utils.testing.basic import BasicRecsystemTest
from renewal_recsystem.utils.testing.functional import (
        RenewalRecsystemFunctionalTest)


class BasicRecsystem(_BasicRecsystem):
    """
    Subclass of the real class modified for testing.

    The only difference is that it uses a lower number of initial articles.
    """

    INITIAL_ARTICLES = 100


class TestPopularityBaseline(BasicRecsystemTest):
    RECSYSTEM_MODULE = 'renewal_recsystem.baseline.popularity'


class TestRandomBaseline(BasicRecsystemTest):
    RECSYSTEM_MODULE = 'renewal_recsystem.baseline.random'


class TestKeywordsBaseline(BasicRecsystemTest):
    RECSYSTEM_MODULE = 'renewal_recsystem.baseline.keywords'

    def test_shutdown(self, recsys):
        """
        After the recsystem is shut down, there should be at least one user
        (from `.BasicRecsystemTest.test_article_interaction`) saved in the
        state dump from the recsystem.
        """

        super().test_shutdown(recsys)

        state_filename = self.RECSYSTEM_MODULE + '.pickle'
        assert os.path.isfile(state_filename)

        with open(state_filename, 'rb') as fobj:
            state = pickle.load(fobj)

        assert 'user_words' in state
        assert len(state['user_words']) >= 1


class TestBaselineRecsystemFunctional(RenewalRecsystemFunctionalTest):
    """
    Run the functional test suite against a running instance of the baseline
    recsystem.
    """

    CMD = [sys.executable, '-m', 'renewal_recsystem',
           'renewal_recsystem.baseline.popularity']
