"""
Runs the tests against the baseline recsystem running in a Docker container.
"""


import os

import pytest

from renewal_recsystem.utils.testing.functional import (
        RenewalRecsystemFunctionalTest)


IMAGE_NAME = 'renewal/recsystems-baseline'
CI_JOB_NAME = 'test:docker'


pytestmark = pytest.mark.skipif(
        (os.environ.get('CI_JOB_NAME') != CI_JOB_NAME
         or 'CI_COMMIT_SHA' not in os.environ),
        reason=f"this test is meant to be run only in the CI though it can "
               f"be simulated by setting the CI_JOB_NAME=docker:test and "
               f"CI_COMMIT_SHA environment variables as long as there is a "
               f"Docker image named {IMAGE_NAME}:<CI_COMMIT_SHA>")


class TestBaselineRecsystemFunctionalDocker(RenewalRecsystemFunctionalTest):
    __doc__ = __doc__ + """

    This test starts the baseline recsystem in a Docker container.  The name
    of the image it runs is ``renewal/recsystems-baseline:<sha1>`` tagged by
    the git commit SHA being tested which comes from the environment.

    See the ``docker`` job in ``.gitlab-ci.yml``.
    """

    # --network=host required so that the container can access the test server
    # running on the host system
    DUMMY_SERVER_HOST = '0.0.0.0'
    DUMMY_SERVER_REMOTE_HOST = os.environ.get('CI_TEMP_CONTAINER_NAME',
                                              'localhost')
    CMD = ['docker', 'run', '--rm']
    RECSYSTEM = 'renewal_recsystem.baseline.random'

    @classmethod
    def get_cmd(cls, uri):
        # For the purposes of this test it sufficies to use any arbitrary
        # (existing) file as the RENEWAL_TOKEN file so long as its contents
        # are simple enough (not multi-line, etc.)
        #
        # If passed the environment variable CI_NETWORK_NAME, the Docker
        # container will be connected to that network.
        # If passed the environment variable CI_TEMP_CONTAINER_NAME, it will
        # be linked to that container.
        cmd = ['-e', f'RENEWAL_API_BASE_URI={uri}',
               '-e', f'RENEWAL_TOKEN=/etc/hostname']

        network_name = os.environ.get('CI_NETWORK_NAME', 'host')
        if network_name:
            cmd.extend(['--network', network_name])

        cmd.append(f'{IMAGE_NAME}:{os.environ["CI_COMMIT_SHA"]}')
        cmd.append(cls.RECSYSTEM)
        return cls.CMD + cmd
