"""
Omnibus test for basic functionality of
`renewal_recsystem.hookmanger.HookManager`.
"""

import asyncio
import itertools
import time

import pytest

from renewal_recsystem.hookmanager import HookManager, method, required


def test_required_method_missing():
    class MyHookManager(HookManager):
        # Define the interfaces for a couple hooks
        # that users can implement
        @required
        def my_task(state, value): pass

    with pytest.raises(RuntimeError,
                       match=r'a hook function with the name and signature '
                       r'my_task\(state, value\) is required'):
        MyHookManager({})


# Hook functions defined for the test_hook_manager test below; they have to
# be defined at the module level so that at least the synchronous hooks can
# be pickled for use with multiprocessing
def my_callback(state, value):
    return {'my_callback': value}


async def my_async_callback(state, value):
    await asyncio.sleep(0.01)
    return {'my_async_callback': value}


def my_method(state, value):
    return value


async def my_async_method(state, value):
    await asyncio.sleep(0.01)
    # Also test that it returns a state update
    return value, {'my_async_method': value}


def every_second(state, message):
    messages = state.get('every_second', [])
    messages.append(message)
    return {'every_second': messages}


async def every_2_seconds(state, message):
    messages = state.get('every_2_seconds', [])
    messages.append(message)
    return {'every_2_seconds': messages}


def background_task(state, n):
    time.sleep(1)
    return {'background_task': n}


async def background_task_async(state, n):
    await asyncio.sleep(1)
    return {'background_task_async': n}


# Used by test_hook_manager; defined at the module-level so that it can
# passed to sub-processes (see issue #24)
class MyHookManager(HookManager):
    # Define the interfaces for a couple hooks
    # that users can implement
    def my_callback(state, value): pass

    def my_async_callback(state, value): pass

    @method
    def my_method(state, value): pass

    @method
    def my_async_method(state, value): pass

    def every_(state, message): pass

    def background_(state, n): pass


def test_hook_manager(event_loop):
    # Now actually implement some hooks as a user might
    hooks = {hook.__name__: hook
             for hook in [my_callback, my_async_callback, my_method,
                          my_async_method, every_second, every_2_seconds,
                          background_task, background_task_async]}

    hm = MyHookManager(hooks)

    def run(coro):
        return event_loop.run_until_complete(coro)

    expected_state = {}
    assert hm._state == expected_state
    # Test basic callbacks/methods
    run(hm.my_callback(value=1))
    expected_state.update({'my_callback': 1})
    assert hm._state == expected_state

    run(hm.my_async_callback(value=1))
    expected_state.update({'my_async_callback': 1})
    assert hm._state == expected_state

    res = run(hm.my_method(value=1))
    assert res == 1
    assert hm._state == expected_state

    # This one also updates the state
    res = run(hm.my_async_method(value=2))
    assert res == 2
    expected_state.update({'my_async_method': 2})
    assert hm._state == expected_state

    # For the background/periodic tasks we let them
    # run for 5 seconds which should be enough time for every_2_seconds to run
    # twice; every_second could run up to 5 times but we give it some cushion
    # and just make sure it ran 4 times
    counter = itertools.count()
    next_n = lambda: next(counter)  # noqa: E731
    background = hm.start_background_hooks(n=next_n)
    periodic = hm.start_periodic_hooks(message='hello')

    try:
        run(asyncio.gather(background, periodic))
        run(asyncio.sleep(5))
    finally:
        # Make sure the tasks are fully shut down
        run(hm.stop_hooks())

    assert asyncio.all_tasks(loop=event_loop) == set()

    # Now check that the state updates from the background tasks are as
    # expected
    assert 'every_second' in hm._state
    assert isinstance(hm._state['every_second'], list)
    assert len(hm._state['every_second']) >= 4
    assert all(m == 'hello' for m in hm._state['every_second'])

    assert 'every_2_seconds' in hm._state
    assert isinstance(hm._state['every_2_seconds'], list)
    assert len(hm._state['every_2_seconds']) >= 2
    assert all(m == 'hello' for m in hm._state['every_2_seconds'])

    # The background tasks should have looped at least 4 or 5 times
    assert hm._state.get('background_task') >= 4
    assert hm._state.get('background_task_async') >= 4
