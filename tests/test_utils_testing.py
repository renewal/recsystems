"""Additional tests for some of the testing utilities."""

import os.path as pth


from renewal_recsystem.utils.testing import (
        websocket_test_server_thread, websocket_test_client,
        WebSocketsMultiClient)


class TestWebsocketsMultiClient:
    """
    Additional tests for
    `~renewal_recsystem.utils.testing.WebsocketsMultiClient`.

    The majority of its functionality is covered by the functional doctest for
    `~renewal_recsystems.server.JSONRPCServerWebsocketClient`.  This just tests
    some additional cases.
    """

    def test_fallback_handler(self, tmpdir_factory):
        """Test calling a custom fallback handler for malformatted messages."""

        bad_messages = []

        async def fallback_handler(message):
            bad_messages.append(message)

        async def client_handler(ws):
            async with WebSocketsMultiClient(
                    ws, fallback_handler=fallback_handler) as rpc_client:
                await rpc_client.square(3)

        async def server_handler(ws, path):
            # Sends bogus reponses to RPC requests; it doesn't necessarily
            # implement the JSON-RPC protocol at all
            await ws.recv()
            await ws.send('bogus message')

        # Start up the server
        sockpath = pth.join(tmpdir_factory.mktemp('unix'), 'ws.sock')
        server_thread = websocket_test_server_thread(server_handler,
                                                     path=sockpath)

        try:
            # run the test client
            websocket_test_client(client_handler, path=sockpath)
        finally:
            server_thread.join(timeout=5)
            assert not server_thread.is_alive()

        assert bad_messages == ['bogus message']
